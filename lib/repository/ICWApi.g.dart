
part of 'ICWApi.dart';

class _IcwApi implements IcwApi {
  _IcwApi(this._dio, {this.baseUrl}) {
    ArgumentError.checkNotNull(_dio, '_dio');
    baseUrl = Strings.BASE_URL;
  }

  final Dio _dio;

  String? baseUrl;

  @override
  Future<RegisterResponse> signUp(FormData jsonEncode) async {
    ArgumentError.checkNotNull(jsonEncode, 'jsonEncode');
    Map<String, dynamic> map = {};
     for (var element in jsonEncode.fields) {
       map[element.key] = element.value;
     }
    final response = await _dio.post(Strings.BASE_URL+Strings.AUTH_SIGNUP,
        queryParameters: map,
        options: Options(
            method: 'POST',
        ));
    RegisterResponse res = RegisterResponse.fromJson(json.decode(response.toString()));
    SharedStorage.save(Strings.SIGN_UP_STORE, res);
    return res;
  }

  @override
  Future<GenericResponse> login(FormData jsonEncode) async {
    ArgumentError.checkNotNull(jsonEncode, 'jsonEncode');
    Map<String, dynamic> map = {};
    for (var element in jsonEncode.fields) {
      map[element.key] = element.value;
    }
    final response = await _dio.request(baseUrl!+Strings.AUTH_LOGIN,
    queryParameters: map,
    options: Options(
      method: 'POST',
     ));
    print("login response is $response");
    GenericResponse res = GenericResponse.fromJson(json.decode(response.toString()));
    return res;
  }

  @override
  Future<UpdateUserResponse> getUser() async {
    Map<String, String> queryParams = await SharedStorage.read(Strings.VALID_USER).then((value) =>
      {'id': GenerateAuthCookieResponse.fromJson(json.decode(value.toString())).user!.id.toString(),
        'u': Strings.adminName,
        'p': Strings.adminPass
      },);
    final response = await _dio.post(baseUrl!+Strings.USER_INFO,
        queryParameters: queryParams,
        options: Options(
          method: 'POST',
        ));
    UpdateUserResponse res = UpdateUserResponse.fromJson(json.decode(response.toString()));
    return res;
  }

  @override
  Future<UpdateUserResponse> updateUser(FormData jsonEncode) async {
    ArgumentError.checkNotNull(jsonEncode, 'jsonEncode');
    Map<String, String> queryParams = await SharedStorage.read(Strings.VALID_USER).then((value) =>
    {'id': "${GenerateAuthCookieResponse.fromJson(json.decode(value.toString())).user!.id}",});
    queryParams.addEntries(jsonEncode.fields);
    final response = await _dio.post(baseUrl!+Strings.AUTH_UPDATE_ACCOUNT,
        queryParameters: queryParams,
        options: Options(
          method: 'POST',
        ));
    UpdateUserResponse res = UpdateUserResponse.fromJson(json.decode(response.toString()));
    return res;
  }

  @override
  Future<NonceResponse> getNonce(String controller, String method) async {
    var currentTime = DateTime.now().millisecondsSinceEpoch;
    Map<String, String> queryParams = {
      'json': 'get_nonce',
      'controller': controller,
      'method': method,
      '_': "$currentTime"
    };
    final response = await _dio.get(baseUrl!+Strings.GET_NONCE,
        queryParameters: queryParams,
        options: Options(
          method: 'GET',
        ));
    NonceResponse res = NonceResponse.fromJson(json.decode(response.toString()));
    return res;
  }

  @override
  Future<GenerateAuthCookieResponse> generateAuthCookie(FormData jsonEncode) async {
    ArgumentError.checkNotNull(jsonEncode, 'jsonEncode');
    Map<String, dynamic> map = {};
    for (var element in jsonEncode.fields) {
      map[element.key] = element.value;
    }
    final response = await _dio.request(baseUrl!+Strings.GET_AUTH_COOKIE,
        queryParameters: map,
        options: Options(
          method: 'POST',
        ));
    GenerateAuthCookieResponse res = GenerateAuthCookieResponse.fromJson(json.decode(response.toString()));
    SharedStorage.save(Strings.VALID_USER, res);
    return res;
  }

  @override
  Future<ValidateAuthCookieResponse> validateAuthCookie(FormData jsonEncode) async {
    ArgumentError.checkNotNull(jsonEncode, 'jsonEncode');
    Map<String, dynamic> map = {};
    for (var element in jsonEncode.fields) {
      map[element.key] = element.value;
    }
    final response = await _dio.request(baseUrl!+Strings.VALIDATE_AUTH_COOKIE,
        queryParameters: map,
        options: Options(
          method: 'POST',
        ));
    ValidateAuthCookieResponse res = ValidateAuthCookieResponse.fromJson(json.decode(response.toString()));
    return res;
  }

  @override
  Future<GenericResponse> resetPassword(FormData jsonEncode) async {
    ArgumentError.checkNotNull(jsonEncode, 'jsonEncode');
    Map<String, dynamic> map = {};
    map.addEntries(jsonEncode.fields);
    final response = await _dio.request(baseUrl!+Strings.RESET_PASSWORD,
        queryParameters: map,
        options: Options(
          method: 'POST',
        ));
    return GenericResponse.fromJson(json.decode(response.toString()));
  }
}