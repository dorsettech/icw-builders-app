import 'dart:convert';

import 'package:icw_sample/Utils/SharedStorage.dart';
import 'package:icw_sample/Utils/Strings.dart';
import 'package:icw_sample/models/GenericResponse.dart';

import 'package:retrofit/retrofit.dart';
import 'package:dio/dio.dart';

import '../models/GenerateAuthCookie.dart';
import '../models/UserInfoResponse.dart';
import '../models/NonceResponse.dart';
import '../models/RegisterResponse.dart';
import '../models/UpdateUserResponse.dart';
import '../models/ValideAuthCookie.dart';

part 'ICWApi.g.dart';

@RestApi(baseUrl: Strings.BASE_URL)
abstract class IcwApi {
  factory IcwApi(Dio dio, {String baseUrl}) = _IcwApi;

  @POST(Strings.BASE_URL)
  Future<GenericResponse> login(FormData jsonEncode);

  @POST(Strings.BASE_URL)
  Future<RegisterResponse> signUp(FormData jsonEncode);

  @GET(Strings.BASE_URL)
  Future<NonceResponse> getNonce(String controller, String method);

  @POST(Strings.BASE_URL)
  Future<UpdateUserResponse> getUser();

  @POST(Strings.BASE_URL)
  Future<UpdateUserResponse> updateUser(FormData jsonEncode);

  @POST(Strings.BASE_URL)
  Future<GenerateAuthCookieResponse> generateAuthCookie(FormData jsonEncode);

  @POST(Strings.BASE_URL)
  Future<ValidateAuthCookieResponse> validateAuthCookie(FormData jsonEncode);

  @POST(Strings.BASE_URL)
  Future<GenericResponse> resetPassword(FormData jsonEncode);
}