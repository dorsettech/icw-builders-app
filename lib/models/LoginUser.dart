import 'package:json_annotation/json_annotation.dart';

@JsonSerializable()
class LoginUser {
  String password;
  String email;

  LoginUser(this.password,this.email,);

  Map<String, dynamic> toJson() => _$PostToJson(this);


  Map<String, dynamic> _$PostToJson(LoginUser instance) =>
      <String, dynamic>{
        'user_password': instance.password,
        'user_login': instance.email,
      };
}