class UpdateUserResponse {
  String? status;
  User? user;

  UpdateUserResponse({this.status, this.user});

  UpdateUserResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.user != null) {
      data['user'] = this.user!.toJson();
    }
    return data;
  }
}

class User {
  Data? data;
  int? iD;
  Caps? caps;
  String? capKey;
  List<String>? roles;
  Allcaps? allcaps;
  Null? filter;

  User(
      {this.data,
        this.iD,
        this.caps,
        this.capKey,
        this.roles,
        this.allcaps,
        this.filter});

  User.fromJson(Map<String, dynamic> json) {
    data = json['data'] != null ? new Data.fromJson(json['data']) : null;
    iD = json['ID'];
    caps = json['caps'] != null ? new Caps.fromJson(json['caps']) : null;
    capKey = json['cap_key'];
    roles = json['roles'].cast<String>();
    allcaps =
    json['allcaps'] != null ? new Allcaps.fromJson(json['allcaps']) : null;
    filter = json['filter'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.data != null) {
      data['data'] = this.data!.toJson();
    }
    data['ID'] = this.iD;
    if (this.caps != null) {
      data['caps'] = this.caps!.toJson();
    }
    data['cap_key'] = this.capKey;
    data['roles'] = this.roles;
    if (this.allcaps != null) {
      data['allcaps'] = this.allcaps!.toJson();
    }
    data['filter'] = this.filter;
    return data;
  }
}

class Data {
  String? iD;
  String? userLogin;
  String? userPass;
  String? userNicename;
  String? userEmail;
  String? userUrl;
  String? userRegistered;
  String? userActivationKey;
  String? userStatus;
  String? displayName;

  Data(
      {this.iD,
        this.userLogin,
        this.userPass,
        this.userNicename,
        this.userEmail,
        this.userUrl,
        this.userRegistered,
        this.userActivationKey,
        this.userStatus,
        this.displayName});

  Data.fromJson(Map<String, dynamic> json) {
    iD = json['ID'];
    userLogin = json['user_login'];
    userPass = json['user_pass'];
    userNicename = json['user_nicename'];
    userEmail = json['user_email'];
    userUrl = json['user_url'];
    userRegistered = json['user_registered'];
    userActivationKey = json['user_activation_key'];
    userStatus = json['user_status'];
    displayName = json['display_name'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['ID'] = this.iD;
    data['user_login'] = this.userLogin;
    data['user_pass'] = this.userPass;
    data['user_nicename'] = this.userNicename;
    data['user_email'] = this.userEmail;
    data['user_url'] = this.userUrl;
    data['user_registered'] = this.userRegistered;
    data['user_activation_key'] = this.userActivationKey;
    data['user_status'] = this.userStatus;
    data['display_name'] = this.displayName;
    return data;
  }
}

class Caps {
  bool? subscriber;

  Caps({this.subscriber});

  Caps.fromJson(Map<String, dynamic> json) {
    subscriber = json['subscriber'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['subscriber'] = this.subscriber;
    return data;
  }
}

class Allcaps {
  bool? read;
  bool? level0;
  bool? subscriber;

  Allcaps({this.read, this.level0, this.subscriber});

  Allcaps.fromJson(Map<String, dynamic> json) {
    read = json['read'];
    level0 = json['level_0'];
    subscriber = json['subscriber'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['read'] = this.read;
    data['level_0'] = this.level0;
    data['subscriber'] = this.subscriber;
    return data;
  }
}