import 'dart:convert';

class UserInfoResponse {
  String? status;
  CurrentUser? user;

  UserInfoResponse({this.status, this.user});

  UserInfoResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    user = json['user'] != null ? new CurrentUser.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    if (this.user != null) {
      data['user'] = this.user!.toJson();
    }
    return data;
  }
}

class CurrentUser {
  int? id;
  String? username;
  String? nicename;
  String? email;
  String? url;
  String? registered;
  String? displayname;
  String? firstname;
  String? lastname;
  String? nickname;
  String? description;
  String? capabilities;
  String? avatar;

  CurrentUser(
      {this.id,
        this.username,
        this.nicename,
        this.email,
        this.url,
        this.registered,
        this.displayname,
        this.firstname,
        this.lastname,
        this.nickname,
        this.description,
        this.capabilities,
        this.avatar});

  CurrentUser.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    username = json['username'];
    nicename = json['nicename'];
    email = json['email'];
    url = json['url'];
    registered = json['registered'];
    displayname = json['displayname'];
    firstname = json['firstname'];
    lastname = json['lastname'];
    nickname = json['nickname'];
    description = json['description'];
    capabilities = json['capabilities'];
    avatar = json['avatar'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['username'] = this.username;
    data['nicename'] = this.nicename;
    data['email'] = this.email;
    data['url'] = this.url;
    data['registered'] = this.registered;
    data['displayname'] = this.displayname;
    data['firstname'] = this.firstname;
    data['lastname'] = this.lastname;
    data['nickname'] = this.nickname;
    data['description'] = this.description;
    data['capabilities'] = this.capabilities;
    data['avatar'] = this.avatar;
    return data;
  }
}