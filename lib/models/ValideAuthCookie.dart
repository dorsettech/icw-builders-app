class ValidateAuthCookieResponse {
  String? status;
  bool? valid;

  ValidateAuthCookieResponse({this.status, this.valid});

  ValidateAuthCookieResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    valid = json['valid'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['valid'] = this.valid;
    return data;
  }
}