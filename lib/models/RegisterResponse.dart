class RegisterResponse {
  String? status;
  String? cookie;
  String? cookieAdmin;
  String? cookieName;
  int? userId;
  String? username;

  RegisterResponse(
      {this.status,
        this.cookie,
        this.cookieAdmin,
        this.cookieName,
        this.userId,
        this.username});

  RegisterResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    cookie = json['cookie'];
    cookieAdmin = json['cookie_admin'];
    cookieName = json['cookie_name'];
    userId = json['user_id'];
    username = json['username'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['status'] = status;
    data['cookie'] = cookie;
    data['cookie_admin'] = cookieAdmin;
    data['cookie_name'] = cookieName;
    data['user_id'] = userId;
    data['username'] = username;
    return data;
  }
}