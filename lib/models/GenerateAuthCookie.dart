class GenerateAuthCookieResponse {
  String? status;
  String? cookie;
  String? cookieName;
  User? user;

  GenerateAuthCookieResponse(
      {this.status, this.cookie, this.cookieName, this.user});

  GenerateAuthCookieResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    cookie = json['cookie'];
    cookieName = json['cookie_name'];
    user = json['user'] != null ? new User.fromJson(json['user']) : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['status'] = this.status;
    data['cookie'] = this.cookie;
    data['cookie_name'] = this.cookieName;
    if (this.user != null) {
      data['user'] = this.user!.toJson();
    }
    return data;
  }
}

class User {
  int? id;
  String? username;
  String? nicename;
  String? email;
  String? url;
  String? registered;
  String? displayname;
  String? firstname;
  String? lastname;
  String? nickname;
  String? description;
  String? capabilities;
  Null? avatar;

  User(
      {this.id,
        this.username,
        this.nicename,
        this.email,
        this.url,
        this.registered,
        this.displayname,
        this.firstname,
        this.lastname,
        this.nickname,
        this.description,
        this.capabilities,
        this.avatar});

  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    username = json['username'];
    nicename = json['nicename'];
    email = json['email'];
    url = json['url'];
    registered = json['registered'];
    displayname = json['displayname'];
    firstname = json['firstname'];
    lastname = json['lastname'];
    nickname = json['nickname'];
    description = json['description'];
    capabilities = json['capabilities'];
    avatar = json['avatar'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['username'] = this.username;
    data['nicename'] = this.nicename;
    data['email'] = this.email;
    data['url'] = this.url;
    data['registered'] = this.registered;
    data['displayname'] = this.displayname;
    data['firstname'] = this.firstname;
    data['lastname'] = this.lastname;
    data['nickname'] = this.nickname;
    data['description'] = this.description;
    data['capabilities'] = this.capabilities;
    data['avatar'] = this.avatar;
    return data;
  }
}