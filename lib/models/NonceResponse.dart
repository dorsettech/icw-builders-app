class NonceResponse {
  String? status;
  String? controller;
  String? method;
  String? nonce;

  NonceResponse({this.status, this.controller, this.method, this.nonce});

  NonceResponse.fromJson(Map<String, dynamic> json) {
    status = json['status'];
    controller = json['controller'];
    method = json['method'];
    nonce = json['nonce'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['status'] = status;
    data['controller'] = controller;
    data['method'] = method;
    data['nonce'] = nonce;
    return data;
  }
}