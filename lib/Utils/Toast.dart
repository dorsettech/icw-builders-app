import 'package:fluttertoast/fluttertoast.dart';

class ShowToast {
  static showToastApp(String? alertMessage) {
    Fluttertoast.showToast(msg: alertMessage.toString());
  }
}