import 'dart:convert';

import 'package:shared_preferences/shared_preferences.dart';

class SharedStorage {

  static Future<dynamic> read(String key) async {
    SharedPreferences _sharedPref = await SharedPreferences.getInstance();
    String? stringValue = _sharedPref.getString(key);
    return stringValue!;
  }

  static readInt(String key) async {
    SharedPreferences _sharedPref = await SharedPreferences.getInstance();
    return _sharedPref.getInt(key);
  }

  static save(String key, value) async {
    SharedPreferences _sharedPref = await SharedPreferences.getInstance();
  _sharedPref.setString(key, json.encode(value));
  }

  static saveInt(String key, value) async {
    SharedPreferences _sharedPref = await SharedPreferences.getInstance();
    _sharedPref.setInt(key, value);
  }

  static remove(String key) async {
    SharedPreferences _sharedPref = await SharedPreferences.getInstance();
    _sharedPref.remove(key);
  }
}