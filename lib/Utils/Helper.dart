import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:icw_sample/Utils/LocalStore.dart';
import 'package:provider/provider.dart';

import '../enum/ApplicationEnums.dart';
import '../ui/screen/HousingScreen.dart';
import '../ui/screen/PdfScreen.dart';
import '../ui/widgets/Dialog.dart';
import 'Strings.dart';

void performAction(BuildContext context, String icon, String from, {int? page}) {
  String title = Strings.HOME;
  switch(icon) {
    case Strings.conversation: if(LocalStore.tabPos == 2) return; title = Strings.CONVERSATION; goToPage(context, title,from); break;
    case Strings.home: if(LocalStore.tabPos == 0) return; goToPage(context, title, from);  break;
    case Strings.pdf:
      title = Strings.PDF_OPERATIONS;
      if(LocalStore.tabPos == 3) return;
      if(from != Strings.fromHome && from != Strings.fromHotSpot && from == Strings.from3DScreen) {
        Navigator.of(context).pushReplacement(
          CupertinoPageRoute(
              builder: (BuildContext context) => PDFScreen(page: page,)),
        );
      } else {
        Navigator.of(context).push(
          CupertinoPageRoute(
              builder: (BuildContext context) => PDFScreen(page: page,)),
        );
      }

      LocalStore.tabPos == 3;
      break;
    case Strings.flats:if(LocalStore.tabPos == 1) return; title = Strings.FLATS; goToPage(context, title,from); break;
  }

}

void goToPage(BuildContext context, String title, String from) {
  var fileIs = "assets/help.html";
  if (title == Strings.HOME) {
    fileIs = "assets/house.html";
  } else if (title == Strings.FLATS) {
    fileIs = "assets/flat.html";
  } else if (title == Strings.CONVERSATION) {
    fileIs = "assets/ship.html";
  }
  if(from != Strings.fromHome && from == Strings.from3DScreen || from == Strings.fromPDF) {
      Navigator.of(context).pushReplacement(
        CupertinoPageRoute(
            builder: (BuildContext context) => HousingScreen(title: title, file: fileIs,)),
      );
  } else {
    Navigator.of(context).push(
      CupertinoPageRoute(
          builder: (BuildContext context) => HousingScreen(title: title,file: fileIs,)),
    );
  }
  LocalStore.tabPos = title == Strings.HOME ? 0 : title == Strings.FLATS ? 1 : title == Strings.CONVERSATION ? 2 : 3;
  // LocalStore.tabPos = title == Strings.HOME ? 0 : title == Strings.FLATS ? 1 : 2;
}

void checkConnectivity(BuildContext context) {
  NetworkStatus networkStatus = Provider.of<NetworkStatus>(context);
  if (networkStatus == NetworkStatus.Online) {
    showAlertDialog(context, "Welcome back! you are connected to the internet..");
  } else {
    showAlertDialog(context,"Oooppsss you are Offline please check your connectivity!");
  }
}

onError(Object obj) {
  final res = (obj as DioError).response;
  // if (res.statusCode == 404) {
  //   ShowToast.showToastApp("error");
  // } else {
  //   ShowToast.showToastApp(res.data["message"]);
  // }
}