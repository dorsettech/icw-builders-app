class Strings {
  static String appName = "The Listener";

  //images
  static String logo = "assets/images/ic_launcher_logo.png";
  static String right_arrow = "assets/images/right-arrow.png";
  static String button_bg = "assets/images/button_back.png";
  static String button_bg_transparent = "assets/images/back_transparent.png";
  static const String conversation = "assets/images/conversation.png";
  static const String flats = "assets/images/flats.png";
  static const String home = "assets/images/home.png";
  static const String pdf = "assets/images/pdf.png";
  static const String ic_lock = "assets/images/ic_pass.png";
  static String ic_key = "assets/images/ic_key.png";
  static String ic_pass_show = "assets/images/ic_pass_visible.png";
  static String ic_bottom_bg = "assets/images/ic_bottom_bg.png";
  static String ic_lock_point = "assets/images/ic_lock_point.png";
  static String ic_user = "assets/images/ic_user.png";
  static String ic_email = "assets/images/ic_email.png";
  static String ic_email_svg = "assets/images/ic_email.svg";
  static String ic_confirm_lock = "assets/images/ic_confirm_lock.png";
  static String ic_user_account = "assets/images/ic_user_account.png";
  static String ic_edit = "assets/images/ic_edit.png";
  static String ic_expand = "assets/images/ic_expand.png";
  static String ic_rotate = "assets/images/ic_rotate.png";
  static String ic_search = "assets/images/ic_search.png";
  static String ic_zoom_in = "assets/images/ic_zoom_in.png";
  static String ic_zoom_out = "assets/images/ic_zoom_out.png";
  static String ic_list = "assets/images/ic_list.png";
  static String ic_exterior = "assets/images/ic_exterior.png";
  static String ic_interior = "assets/images/ic_interior.png";
  static String ic_next = "assets/images/ic_next.png";
  static String ic_back = "assets/images/ic_back.png";
  static String ic_download = "assets/images/ic_download.png";
  static String ic_hide_pass = "assets/images/hide-password.png";
  static String url = "https://pdfkit.org/docs/guide.pdf";

  //labels
  static const String BOOK_MARK = "bookMark";
  static const String LOGIN_LABEL = "Username or Email";
  static const String NAME = "Name";
  static const String USERNAME_LABEL = "Username";
  static const String EMAIL_LABEL = "Email";
  static const String PASSWORD_LABEL = "Password";
  static const String CONFIRM_PASSWORD_LABEL = "Confirm Password";
  static const String CREATE_ACCOUNT_LABEL = "Create Account";
  static const String LETS_START = "Get Started";
  static const String LOGIN = "Login";
  static const String SAVE = "Save";
  static const String SIGN_UP = "Register";
  static const String FORGOT_PASS = "Forgot Password?";
  static const String SUBMIT = "Submit";
  static const String RESET_PASS = "Reset Password";
  static const String NOT_REGISTERED_CREATE_ACCOUNT = "Not registered? Create an account";
  static const String SUCCESS = "Success!";
  static const String SUCCESS_MESSAGE = "Your registration has been successful.";
  static const String REGISTERATION_HEADER = "Registration successful";
  static const String MY_ACCOUNT = "My Account";
  static const String SUCCESS_MESSAGE_DETAIL = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.";
  static const String KEY_MEASURE = "Key Measures";
  static const String ICW_MANUAL = "ICW Technical Manual";
  static const String KEY_ICW_DETAIL = "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s.";
 static const String HOME = "Housing";
 static const String FLATS = "Flats";
 static const String CONVERSATION = "Conversion";
 static const String PDF_READER = "PDF Reader";
 static const String PDF_OPERATIONS = "Operations PDF";
 static const String CHANGE_PASSWORD = "Change Password";
 static const String EXTERIOR = "Exterior";
 static const String INTERIOR = "Interior";
 static const String LIST = "List";
 static const String TITLES = "Titles";
 static const String alert = "Alert";
 static const String ok = "ok";

 //local Store
  static const String SIGN_UP_STORE = "signup";
  static const String VALID_USER = "validateUser";
  static const String USER_DATA = "userData";

  //validations
  static const String fill_all_field = "Please Fill all fields";
  static const String enter_email = "Please Enter your email";
  static const String enter_password = "Please Enter your password";
  static const String invalid_password = "Please Enter password greater then 6 and Password contain numbers and letters";
  static const String invalid_email = "Please enter a valid email";
  static const String pass_not_match = "Password does not match";
  static const String invalid_name = "Name contain only alphabets";


  //colors
  static int SKY_COLOR = 0xff7895c4;
  static int LOGIN_COLOR = 0xff48b9c1;
  static int INVISIBLE = 0x0000ffff;

  //margin or paddings etc
  static double margin_twenty = 20;
  static double margin_thirty = 30;
  static double margin_fourty = 40;
  static double margin_fifty = 50;
  static double margin_sixty = 60;
  static double margin_seventy =70;
  static double margin_eighty = 80;


  //const
  static const String fromHome = "fromHome";
  static const String fromHotSpot = "fromHotSpot";
  static const String from3DScreen = "from3DScreen";
  static const String fromPDF = "fromPDF";
  static const String fromTitles = "fromTitle";
  static const String adminName = "icwappsupport";
  static const String adminPass = "tr6?r0cnH1ThWros";

  //Api calls
  static const String BASE_URL = "https://icw.honesttech.co.uk/api/";
  static const String AUTH_TOKEN = "";
  static const String AUTH_LOGIN = "users/login/";
  static const String AUTH_SIGNUP = "user/register/";
  static const String AUTH_FORGOT_PASS = "";
  static const String AUTH_UPDATE_ACCOUNT = "users/update_user";
  static const String USER_INFO = "users/get_userdata/";
  static const String GET_NONCE = "get_nonce/";
  static const String GET_AUTH_COOKIE = "auth/generate_auth_cookie/";
  static const String VALIDATE_AUTH_COOKIE = "user/validate_auth_cookie/";
  static const String RESET_PASSWORD = "user/retrieve_password/";

  //pdf titles
  static final List<Map<String, dynamic>> allUsers = [
    {"id": 1, "name": "Introduction", "age": 10},
    {"id": 2, "name": "Suitability of Materials and Workmanship", "age": 15},
    {"id": 3, "name": "Guidance for Design and Construction Methods", "age": 17},
    {"id": 4, "name": "Inspections Introduction", "age": 20},
    {"id": 5, "name": "Site and Construction Overview", "age": 21},
    {"id": 6, "name": "Excavations", "age": 25},
    {"id": 7, "name": "Foundation to Ground Floor", "age": 26},
    {"id": 8, "name": "Ground Floor to Upper Floor", "age": 28},
    {"id": 9, "name": "Upper Floors to Roof Structure", "age": 32},
    {"id": 10, "name": "Pre Plaster and Plaster Board", "age": 33},
    {"id": 11, "name": "Completion", "age": 39},
    {"id": 12, "name": "Site Investigation", "age": 46},
    {"id": 13, "name": "Foundations", "age": 52},
    {"id": 14, "name": "Strip and Trench Foundations", "age": 53},
    {"id": 15, "name": "Minimum Depth of Strip Foundations", "age": 54},
    {"id": 16, "name": "Concrete Mix", "age": 55},
    {"id": 17, "name": "Stepped Foundations", "age": 57},
    {"id": 18, "name": "Engineered Foundations", "age": 58},
    {"id": 19, "name": "Engineered Fill", "age": 58},
    {"id": 20, "name": "Piled Foundations", "age": 59},
    {"id": 21, "name": "Raft Foundation", "age": 60},
    {"id": 22, "name": "Foundations and Trees", "age": 61},
    {"id": 23, "name": "Construction Principles", "age": 63},
    {"id": 24, "name": "Substructure", "age": 66},
    {"id": 25, "name": " Walls below DPC", "age": 67},
    {"id": 26, "name": "DPC", "age": 68},
    {"id": 27, "name": "Service Penetrations", "age": 69},
    {"id": 28, "name": "Pipes Bedded into Walls", "age": 69},
    {"id": 29, "name": " Pipes Lintelled through Walls", "age": 70},
    {"id": 30, "name": " Waterproofing Design", "age": 71},
    {"id": 31, "name": " Types of Waterproofing", "age": 72},
    {"id": 32, "name": " Drainage", "age": 76},
    {"id": 33, "name": " Excavation", "age": 77},
    {"id": 34, "name": " Access", "age": 78},
    {"id": 35, "name": " Connection", "age": 80},
    {"id": 36, "name": " Gullies", "age": 80},
    {"id": 37, "name": " Bedding Material", "age": 81},
    {"id": 38, "name": " Backfill", "age": 81},
    {"id": 39, "name": " Flexible Pipes", "age": 82},
    {"id": 40, "name": " Drainage System", "age": 83},
    {"id": 41, "name": " Above Ground Drainage", "age": 84},
    {"id": 42, "name": "Drainage Layout", "age": 85},
    {"id": 43, "name": "Ventilation", "age": 86},
    {"id": 44, "name": "Testing", "age": 87},
    {"id": 45, "name": "Ground Floor", "age": 90},
    {"id": 46, "name": "Ground Bearing Slab", "age": 91},
    {"id": 47, "name": "Precast Beam and Block Floors", "age": 93},
    {"id": 48, "name": "Suspended Timber Floors", "age": 93},
    {"id": 49, "name": "Damp Proof Membrane", "age": 94},
    {"id": 50, "name": "Radon Gas Barrier", "age": 95},
    {"id": 51, "name": "Superstructure", "age": 98},
    {"id": 52, "name": "Exposure", "age": 99},
    {"id": 53, "name": "Bricks", "age": 100},
    {"id": 54, "name": "Blocks", "age": 100},
    {"id": 55, "name": "Masonry Protection", "age": 101},
    {"id": 56, "name": "Mortar", "age": 102},
    {"id": 57, "name": " General Advice on Cavity Wall", "age": 102},
    {"id": 58, "name": " Solid Walls", "age": 103},
    {"id": 59, "name": "Cavity Walls", "age": 104},
    {"id": 60, "name": "Framed Walls", "age": 105},
    {"id": 61, "name": "Lateral Restraint", "age": 106},
    {"id": 62, "name": "Lateral Restraint Straps", "age": 107},
    {"id": 63, "name": "Wall Ties", "age": 107},
    {"id": 64, "name": "Cavity Closers", "age": 108},
    {"id": 65, "name": "Movement Joints", "age": 109},
    {"id": 66, "name": "Structural Openings", "age": 110},
    {"id": 67, "name": "Cavity Trays & DPC", "age": 111},
    {"id": 68, "name": "Floor Joists", "age": 112},
    {"id": 69, "name": "Notching & Drilling", "age": 113},
    {"id": 70, "name": " Floor Decking", "age": 115},
    {"id": 71, "name": " Particle Boarding", "age": 115},
    {"id": 72, "name": "Staircases", "age": 116},
    {"id": 73, "name": "Internal Walls", "age": 117},
    {"id": 74, "name": "Fire Resistance", "age": 119},
    {"id": 75, "name": "Sound Insulation", "age": 120},
    {"id": 76, "name": "Pre Completion Testing", "age": 121},
    {"id": 77, "name": "Robust Details", "age": 121},
    {"id": 78, "name": "Chimneys", "age": 122},
    {"id": 79, "name": "Cladding Introduction", "age": 126},
    {"id": 80, "name": "Timber Boarding", "age": 127},
    {"id": 81, "name": "Timber", "age": 130},
    {"id": 82, "name": "Plywood", "age": 131},
    {"id": 83, "name": "Render on to Boarding", "age": 131},
    {"id": 84, "name": "Tile and Slate Cladding", "age": 132},
    {"id": 85, "name": "Other Cladding", "age": 133},
    {"id": 86, "name": "Weather Resistance of Walls and Cladding", "age": 133},
    {"id": 87, "name": "External Treatment", "age": 134},
    {"id": 88, "name": "Internal Treatment", "age": 134},
    {"id": 89, "name": "Interstitial Condensation", "age": 135},
    {"id": 90, "name": "Surface Condensation", "age": 135},
    {"id": 91, "name": " Control of Moisture Penetration", "age": 136},
    {"id": 92, "name": "Thermal Insulation of Walls and Claddings", "age": 137},
    {"id": 93, "name": "Render Application", "age": 138},
    {"id": 94, "name": "Finishes", "age": 139},
    {"id": 95, "name": "Further Guidance", "age": 140},
    {"id": 96, "name": "Roofs", "age": 144},
    {"id": 97, "name": "Bracing & Lateral Restraint", "age": 145},
    {"id": 98, "name": "Fire Stopping", "age": 147},
    {"id": 99, "name": "Roof Coverings", "age": 148},
    {"id": 100, "name": "Timber Frame", "age": 154},
    {"id": 101, "name": "Timber Frame Introduction", "age": 155},
    {"id": 102, "name": "Substructure", "age": 156},
    {"id": 103, "name": "Sole Plate", "age": 156},
    {"id": 104, "name": " Sheathing Boards", "age": 157},
    {"id": 105, "name": "Fixing", "age": 158},
    {"id": 106, "name": "Breather Membrane", "age": 159},
    {"id": 107, "name": " Fire Stopping", "age": 160},
    {"id": 108, "name": "Internal", "age": 161},
    {"id": 109, "name": "Modern Methods of Construction", "age": 164},
    {"id": 110, "name": "Internal Services", "age": 167},
    {"id": 111, "name": "Space Heating", "age": 172},
    {"id": 112, "name": "Escape Window", "age": 174},
    {"id": 113, "name": "Safety Glazing", "age": 175},
    {"id": 114, "name": "Inspection Process", "age": 178},
    {"id": 115, "name": "Site and Construction Overview", "age": 179},
    {"id": 116, "name": "Foundation to Ground Floor", "age": 183},
    {"id": 117, "name": "Strip Out and Assessment of Original Structure", "age": 185},
    {"id": 118, "name": "Superstructure to Upper Floor", "age": 187},
    {"id": 119, "name": "Upper Floors to Pre-Plaster including Roof Structure", "age": 191},
    {"id": 120, "name": "Completion", "age": 198},
    {"id": 121, "name": "Conversion Introduction", "age": 206},
    {"id": 122, "name": "Assessment Process", "age": 208},
    {"id": 123, "name": "Substructure", "age": 209},
    {"id": 124, "name": "Superstructure", "age": 216},
  ];

}