
class Validator {

  static bool checkEmail(String? email) {
    bool emailValid = RegExp(r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+").hasMatch(email!);
    return emailValid;
  }

  static bool checkName(String? name) {
    bool validName = RegExp('[a-zA-Z]').hasMatch(name!);
    return validName;
  }
  
  static bool checkPassword(String? pass) {
    bool password = (RegExp(r'^[A-Za-z0-9_.]+$').hasMatch(pass!));
    if (pass.length > 6 && password) {
      return true;
    } else {
      return false;
    }
  }

}
