import 'package:confetti/confetti.dart';
import 'package:flutter/cupertino.dart';

Align buildConfettiWidget(controller, double blastDirection) {
  return Align(
    alignment: Alignment.topCenter,
    child: ConfettiWidget(
      maximumSize: Size(30, 30),
      shouldLoop: false,
      confettiController: controller,
      blastDirection: blastDirection,
      blastDirectionality: BlastDirectionality.directional,
      maxBlastForce: 20, // set a lower max blast force
      minBlastForce: 8, // set a lower min blast force
      emissionFrequency: 1,
      numberOfParticles: 8, // a lot of particles at once
      gravity: 1,
    ),
  );
}