import 'package:flutter/material.dart';
import 'package:icw_sample/Utils/Strings.dart';

showAlertDialog(BuildContext context,String message) {

  // set up the button
  Widget okButton = TextButton(
    child: const Text(Strings.ok),
    onPressed: () {
      Navigator.of(context).pop();
    },
  );

  // set up the AlertDialog
  AlertDialog alert = AlertDialog(
    title: const Text(Strings.alert),
    content: Text(message),
    actions: [
      okButton,
    ],
  );

  // show the dialog
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return alert;
    },
  );
}