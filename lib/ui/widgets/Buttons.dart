import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:icw_sample/Utils/Toast.dart';
import 'package:icw_sample/provider/AccountProvider.dart';
import 'package:icw_sample/provider/EmailProvider.dart';
import 'package:icw_sample/provider/LoginProvider.dart';
import 'package:icw_sample/provider/RegisterProvider.dart';
import 'package:icw_sample/streams/ButtonStreams.dart';
import 'package:icw_sample/ui/screen/HousingScreen.dart';
import 'package:icw_sample/ui/screen/Login.dart';
import 'package:icw_sample/ui/screen/TitlesListScreen.dart';
import 'package:icw_sample/ui/widgets/CustomWidgets.dart';
import 'package:tuple/tuple.dart';

import '../../Utils/Helper.dart';
import '../../Utils/Strings.dart';
import '../screen/Dashboard.dart';

Widget loginButton(BuildContext context, String text, double b, double t, Object provider, {Tuple2<TextEditingController, TextEditingController>? controllers, Tuple5<TextEditingController,TextEditingController,TextEditingController,TextEditingController,TextEditingController>? signUpControllers, TextEditingController? emailController}) {
  return Container(
      margin: EdgeInsets.only(top: t, bottom: b),
      width: MediaQuery.of(context).size.width,
      child: Bouncing(child: Stack(
        alignment: Alignment.center,
        children: [
          SizedBox(
            height: Strings.margin_eighty,
            width: 300,
            child: Image.asset(
              "assets/images/parallelogram.png",
              color: Color(Strings.LOGIN_COLOR),
              fit: BoxFit.fill,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                text,
                style: const TextStyle(color: Colors.white70),
              ),
              Image.asset(Strings.right_arrow,
                  height: Strings.margin_thirty,
                  width: Strings.margin_fifty,
                  color: Colors.white70)
            ],
          )
        ],
      ), onPress: () {
        switch (text) {
          case Strings.LOGIN:
            (provider as LoginProvider).checkUserValidity(controllers, context);
            break;
          case Strings.LETS_START:
            Navigator.of(context).pushReplacement(
              CupertinoPageRoute(
                  builder: (BuildContext context) => const Dashboard()),
            );
            break;
          case Strings.SUBMIT:
            (provider as EmailProvider).checkEmailValidity(emailController, context);
            break;
          case Strings.CREATE_ACCOUNT_LABEL:
            (provider as RegisterProvider).checkUserSignUpCredentials(context, signUpControllers);
            break;
          case Strings.SAVE:
            (provider as AccountProvider).checkUserSignUpCredentials(context, signUpControllers);
            break;
        }
      },)
  );
}

Widget back(BuildContext context, double b, double t) {
  return Container(
      margin: EdgeInsets.only(left: Strings.margin_twenty, top: t, bottom: b),
      child: GestureDetector(
        child: const Icon(Icons.arrow_back),
        onTap: () {
          Navigator.pop(context);
        },
      ));
}

Widget account(BuildContext context, double b, double t) {
  return Container(
      margin: EdgeInsets.only(right: Strings.margin_twenty, top: t, bottom: b),
      child: Image.asset(Strings.ic_user_account, width: 28));
}

Widget bottomSheet(BuildContext context, String icon, String from) {
  return Bouncing(
    onPress: () {
      performAction(context, icon, from);
    },
    child: Image.asset(
      icon,
      color: Colors.white,
      width: 30,
    ),);
}

Widget viewIntOrExt3D(BuildContext context) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceAround,
    children: [
      Column(
        children: [
          IconButton(
            icon: Image.asset(
              Strings.ic_exterior,
              width: 25,
            ),
            onPressed: () {},
          ),
          const Text(Strings.EXTERIOR),
        ],
      ),
      // Column(children: [
      //   IconButton(
      //     icon: Image.asset(
      //       Strings.ic_interior,
      //       width: 25,
      //     ),
      //     onPressed: () {},
      //   ),
      //   const Text(Strings.INTERIOR),
      // ]),
      Column(
        children: [
          IconButton(
            icon: Image.asset(
              Strings.ic_list,
              width: 25,
            ),
            onPressed: () {
              Navigator.of(context).push(
                CupertinoPageRoute(
                    builder: (BuildContext context) => const TitlesListScreen()),
              );
            },
          ),
          const Text(Strings.LIST)
        ],
      )
    ],
  );
}

Widget actions(String image, double size) {
  return Container(padding: const EdgeInsets.all(8), child:IconButton(onPressed: () {}, icon: Image.asset(image, width: size,), iconSize: size,));
}