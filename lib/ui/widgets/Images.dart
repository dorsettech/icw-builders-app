
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:icw_sample/ui/widgets/CustomWidgets.dart';

import '../../Utils/Strings.dart';
import '../screen/UserAccount.dart';
import 'Buttons.dart';

Widget loginLogo(BuildContext context, double bottom, double h, double w,
    double t) {
  return Container(
      margin: EdgeInsets.fromLTRB(0, t, 0, bottom),
      width: MediaQuery
          .of(context)
          .size
          .width,
      child: Image.asset(
        Strings.logo,
        height: h,
        width: w,
      )
  );
}

Widget bg() {
  return SizedBox(
    height: 200,
    width: 200,
    child: Stack(
      children: [
        Container(
          margin: const EdgeInsets.only(left: 8, top: 8),
          child: Image.asset(Strings.button_bg_transparent,),
        ),
        Image.asset(Strings.button_bg),
      ],
    ),
  );
}

Widget dashboardImages(String image, String title) {
  return Container(
    margin: const EdgeInsets.only(top: 20),
      child: Column(
    children: [
      Image.asset(image, color: Colors.white, width: 60, height: 60,),
      Text(title, style: const TextStyle(color: Colors.white, fontWeight: FontWeight.bold,),)
    ],
  ));
}

Widget myAccount(BuildContext context) {
 return Bouncing(
    child: account(context, 0, 0),
    onPress: () {
      Navigator.of(context).push(
        CupertinoPageRoute(
            builder: (BuildContext context) => const UserAccount()),
      );
    },
  );
}