import 'package:flutter/cupertino.dart';

import '../../Utils/Strings.dart';
import 'Buttons.dart';
import 'Text.dart';
import 'package:flutter/material.dart';

Widget bottomSheetDashboard(BuildContext context, String from) {
  return SizedBox(
      width: MediaQuery.of(context).size.width,
      child: Stack(
        alignment: Alignment.center,
        children: [
          SizedBox(
            width: MediaQuery.of(context).size.width,
            child: Image.asset(
              Strings.ic_bottom_bg,
              fit: BoxFit.fill,
              height: 80,
            ),
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              bottomSheet(context, Strings.home, from),
              bottomSheet(context, Strings.flats, from),
              bottomSheet(context, Strings.conversation, from),
              bottomSheet(context, Strings.pdf, from),
            ],
          ),
        ],
      ));
}

Widget appBar(BuildContext context, String title) {
  return Row(
    mainAxisAlignment: MainAxisAlignment.spaceAround,
    children: [
      back(context, 0, 0),
      Expanded(
        child: Container(
            margin: const EdgeInsets.only(bottom: 40, right: 30),
            child: AuthHeader(title)),
      ),
    ],
  );
}

class AnimatedButton extends AnimatedWidget {
  final AnimationController _controller;
  const AnimatedButton({
    required AnimationController controller,
  })  : _controller = controller,
        super(listenable: controller); // (a).

  @override
  Widget build(BuildContext context) {
    return Transform.scale( // (b).
      scale: 1 - _controller.value, // (c).
      child: Container(
        height: 70,
        width: 200,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20.0),
            boxShadow: const [
              BoxShadow(
                color: Color(0x80000000),
                blurRadius: 10.0,
                offset: Offset(0.0, 2.0),
              ),
            ],
            gradient: const LinearGradient(
              colors: [
                Color(0xff00e6dc),
                Color(0xff00ffb9),
              ],
            )),
        child: const Center(
          child: Text('Press button',
              style: TextStyle(
                fontSize: 20.0,
                fontWeight: FontWeight.bold,
                color: Color(0xff000028),
              )),
        ),
      ),
    );
  }
}


class Bouncing extends StatefulWidget {
  final Widget child;
  final VoidCallback onPress;

  const Bouncing({required this.child, Key? key, required this.onPress})
      : super(key: key);

  @override
  _BouncingState createState() => _BouncingState();
}

class _BouncingState extends State<Bouncing>
    with SingleTickerProviderStateMixin {
  late double _scale;
  late AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 150),
      lowerBound: 0.0,
      upperBound: 0.1,
    );
    _controller.addListener(() {
      setState(() {});
    });
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _scale = 1 - _controller.value;
    return Listener(
      onPointerDown: (PointerDownEvent event) {
        if (widget.onPress != null) {
          _controller.forward();
        }
      },
      onPointerUp: (PointerUpEvent event) {
        if (widget.onPress != null) {
          _controller.reverse();
          widget.onPress();
        }
      },
      child: Transform.scale(
        scale: _scale,
        child: widget.child,
      ),
    );
  }
}