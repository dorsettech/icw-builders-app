import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:icw_sample/provider/LoginProvider.dart';
import 'package:icw_sample/provider/RegisterProvider.dart';

import '../../Utils/Strings.dart';
import '../../provider/AccountProvider.dart';

Widget InputField(TextEditingController controller, String login_label,
    String icon, FocusNode _focusNode, Color color,
    {String? passShowOrHide, Object? provider}) {
  return Container(
    margin: EdgeInsets.symmetric(horizontal: 30),
    child: TextFormField(
      obscureText: login_label == Strings.PASSWORD_LABEL ||
              login_label == Strings.CONFIRM_PASSWORD_LABEL
          ? passShowOrHide == Strings.ic_pass_show
              ? false
              : true
          : false,
      focusNode: _focusNode,
      controller: controller,
      textInputAction: TextInputAction.next,
      decoration: InputDecoration(
        border: UnderlineInputBorder(),
        prefixIcon: Padding(
          padding: const EdgeInsetsDirectional.only(end: 6.0),
          child: Image.asset(icon), // myIcon is a 48px-wide widget.
        ),
        suffixIcon: login_label == Strings.PASSWORD_LABEL ||
                login_label == Strings.CONFIRM_PASSWORD_LABEL
            ? Padding(
                padding: const EdgeInsetsDirectional.only(end: 6.0),
                child: GestureDetector(
                  child: Image.asset(passShowOrHide!),
                  onTap: () {
                    if (provider is LoginProvider) {
                      (provider).passVisibility(passShowOrHide: passShowOrHide);
                    } else if (provider is RegisterProvider) {
                      if (login_label != Strings.CONFIRM_PASSWORD_LABEL) {
                        (provider)
                            .passVisibility(passShowOrHide: passShowOrHide);
                      } else {
                        (provider).confirmPassVisibility(
                            passShowOrHide: passShowOrHide);
                      }
                    }
                  },
                ) // myIcon is a 48px-wide widget.
                )
            : Container(),
        suffixIconConstraints: BoxConstraints(
          maxWidth: 30,
        ),
        prefixIconConstraints: BoxConstraints(
          maxWidth: 30,
        ),
        labelStyle: TextStyle(
          color: _focusNode.hasFocus ? Color(Strings.LOGIN_COLOR) : Colors.grey,
        ),
        labelText: login_label,
      ),
    ),
  );
}

Widget InputFieldAccUpdate(TextEditingController controller, String login_label,
    String icon, FocusNode _focusNode, Color color, String suffixIcon, {AccountProvider? provider, int? i}) {
  return Container(
    margin: EdgeInsets.symmetric(horizontal: 30),
    child: TextFormField(
      focusNode: _focusNode,
      controller: controller,
      textInputAction: TextInputAction.next,
      decoration: InputDecoration(
        border: UnderlineInputBorder(),
        prefixIcon: Padding(
          padding: const EdgeInsetsDirectional.only(end: 6.0),
          child: Image.asset(icon), // myIcon is a 48px-wide widget.
        ),
        suffixIcon: Padding(
          padding: const EdgeInsetsDirectional.only(end: 6.0),
          child: Image.asset(suffixIcon), // myIcon is a 48px-wide widget.
        ),
        suffixIconConstraints: BoxConstraints(maxWidth: 30),
        prefixIconConstraints: BoxConstraints(
          maxWidth: 30,
        ),
        labelStyle: TextStyle(
          color: _focusNode.hasFocus ? Color(Strings.LOGIN_COLOR) : Colors.grey,
        ),
        labelText: login_label,
      ),
      onChanged: (_) {
        switch(i) {
          case 1:
            provider?.response?.data?.displayName = controller.text.toString();
            break;
          case 2:
            provider?.response?.data?.userLogin = controller.text.toString();
            break;
          case 3:
            provider?.response?.data?.userEmail = controller.text.toString();
            break;
        }
      },
    ),
  );
}
