import 'package:flutter/cupertino.dart';

Widget AuthHeader(String header) {
  return Container(
    margin: EdgeInsets.only(top: 40),
    child: Text(
      header,
      textAlign: TextAlign.center,
      style: TextStyle(fontWeight: FontWeight.bold),
    ),
  );
}

Widget registrationSuccess(String title) {
  return Container(
    margin: EdgeInsets.only(top: 40),
    child: Text(
      title,
      textAlign: TextAlign.center,
      style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold),
    ),
  );
}

Widget anyText(String text, double fontSize, double t, double b, FontWeight weight) {
  return Container(
    margin: EdgeInsets.only(top: t, bottom: b),
    child: Text(
      text,
      textAlign: TextAlign.center,
      style: TextStyle(fontSize: fontSize, fontWeight: weight),
    ),
  );
}