import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:icw_sample/ui/widgets/Text.dart';
import 'package:no_internet_check/internet_connectivity/initialize_internet_checker.dart';

import '../../Utils/Helper.dart';
import '../../Utils/Strings.dart';
import '../widgets/Buttons.dart';
import '../widgets/CustomWidgets.dart';
import '../widgets/Images.dart';

class TitlesListScreen extends StatefulWidget {
  const TitlesListScreen({
    Key? key,
  }) : super(key: key);

  @override
  _TitlesListScreen createState() => _TitlesListScreen();
}

class _TitlesListScreen extends State<TitlesListScreen> {
  List<int>? myList = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];

  List<Map<String, dynamic>> _foundUsers = [];

  TextEditingController editingController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _foundUsers = Strings.allUsers;
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          if (Navigator.of(context).userGestureInProgress) {
            return false;
          } else {
            return true;
          }
        },
        child: Scaffold(
            body:
                Column(mainAxisAlignment: MainAxisAlignment.center, children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: [
                      Container(
                          margin: EdgeInsets.only(top: 50),
                          child: back(context, 0, 0)),
                      Expanded(
                        child: Container(
                            margin: const EdgeInsets.only(bottom: 40, top: 50),
                            child: AuthHeader(Strings.TITLES)),
                      ),
                      Container(child: myAccount(context), margin: EdgeInsets.only(top: 50),)
                    ],
                  ),
          Padding(
            padding: const EdgeInsets.all(1.0),
            child: TextField(
              onChanged: (value) {
                _runFilter(value);
              },
              controller: editingController,
              decoration: const InputDecoration(
                  labelText: "Search",
                  prefixIcon: Icon(Icons.search),
                  border: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(25.0)))),
            ),
          ),
          Expanded(
            child: _foundUsers.isNotEmpty
                ? ListView.builder(
                    itemCount: _foundUsers.length,
                    itemBuilder: (context, index) => Card(
                      key: ValueKey(_foundUsers[index]["id"]),
                      color: Color(Strings.LOGIN_COLOR),
                      elevation: 8,
                      margin: const EdgeInsets.symmetric(vertical: 5,horizontal: 5),
                      child: ListTile(
                        leading: Text(
                          _foundUsers[index]["id"].toString(),
                          style: const TextStyle(fontSize: 24),
                        ),
                        title: Text(_foundUsers[index]['name']),
                        onTap: () {
                          performAction(context, Strings.pdf, Strings.fromTitles ,page: _foundUsers[index]["age"]);
                        },
                      ),
                    ),
                  )
                : const Text(
                    'No results found',
                    style: TextStyle(fontSize: 24),
                  ),
          ),
        ])));
  }

  // This function is called whenever the text field changes
  void _runFilter(String enteredKeyword) {
    List<Map<String, dynamic>> results = [];
    if (enteredKeyword.isEmpty) {
      // if the search field is empty or only contains white-space, we'll display all users
      results = Strings.allUsers;
    } else {
      results = Strings.allUsers
          .where((user) =>
              user["name"].toLowerCase().contains(enteredKeyword.toLowerCase()))
          .toList();
      // we use the toLowerCase() method to make it case-insensitive
    }

    // Refresh the UI
    setState(() {
      _foundUsers = results;
    });
  }

// void validation() {
//   if (_email.text.isNotEmpty && _password.text.isNotEmpty) {
//     if (Validator.emailValid(_email.text)) {
//       if (_password.text.length > 5) {
//         if (patient)
//           loginPatient();
//         else
//           loginDoctor();
//       } else {
//         loginDoctor();
//         ShowToast.showToastApp(Strings.passwordShort, context);
//       }
//     } else {
//       ShowToast.showToastApp(Strings.emailNotValid, context);
//     }
//   } else {
//     ShowToast.showToastApp(Strings.fillAllFields, context);
//   }
// }

// void loginPatient() {
//   showDialog(
//     context: context,
//     barrierDismissible: false,
//     builder: (BuildContext context) {
//       return Spinner.spinkit;
//     },
//   );
//   var data = LoginUser(_password.text.trim(), _email.text.trim());
//   final client =
//   LoginClient(Dio(BaseOptions(contentType: "application/json")));
//   client.loginUser(jsonEncode(data.toJson())).then((value) async {
//     savePatientData(value);
//     Navigator.of(context).pushReplacement(
//       CupertinoPageRoute(builder: (BuildContext context) => ShowUserMeditationOrBookSession()),
//     );
//   }).catchError((Object obj) {
//     Navigator.pop(context);
//     switch (obj.runtimeType) {
//       case DioError:
//         onError(obj);
//         break;
//       default:
//     }
//   });
// }

// onError(Object obj) {
//   final res = (obj as DioError).response;
//   if (res.statusCode == 404) {
//     ShowToast.showToastApp("error");
//   } else {
//     ShowToast.showToastApp(res.data["message"]);
//   }
// }

// saveUserData(LoginUserResponse value) {
//   SharedPref sharedPref = SharedPref();
//   value.password = _password.text.trim();
//   sharedPref.save("user", value);
//   ShowToast.showToastApp("Login Successful", context);
//   Navigator.pop(context);
// }
}
