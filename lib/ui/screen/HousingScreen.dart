import 'dart:convert';
import 'dart:io';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:icw_sample/ui/widgets/CustomWidgets.dart';
import 'package:no_internet_check/internet_connectivity/initialize_internet_checker.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:webview_flutter_plus/webview_flutter_plus.dart';

import '../../Utils/Helper.dart';
import '../../Utils/LocalStore.dart';
import '../../Utils/Strings.dart';
import '../widgets/Buttons.dart';
import '../widgets/Images.dart';
import '../widgets/Text.dart';
import 'ModelFullScreen.dart';
import 'TitlesListScreen.dart';

class HousingScreen extends StatefulWidget {
  final String title;
  final String file;

  const HousingScreen({Key? key, required this.title, required this.file}) : super(key: key);

  @override
  _HousingScreen createState() => _HousingScreen();
}

class _HousingScreen extends State<HousingScreen>
    with SingleTickerProviderStateMixin {
  late WebViewPlusController _controller;
  bool _initialized = false;

  @override
  void initState() {
    InternetChecker();
    super.initState();
    WidgetsBinding.instance?.addPostFrameCallback((_) {
      _initialize();
    });
  }

  void _initialize() {
    Future<void>.delayed(const Duration(seconds: 1), () {
      if (mounted) { // Check that the widget is still mounted
        setState(() {
          _initialized = true;
        });
      }
    });
  }

  @override
  void dispose() {
    LocalStore.tabPos = -1;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
     if (!_initialized) {
       return WillPopScope(
           onWillPop: () async {
         if (Navigator.of(context).userGestureInProgress) {
           return false;
         } else {
           return true;
         }
       },
    child: const Scaffold(
    backgroundColor: Colors.white,
    body: Center(
    child: CircularProgressIndicator(),
    )));
    } else {
      return  WillPopScope(
           onWillPop: () async {
             if (Navigator.of(context).userGestureInProgress) {
               return false;
             } else {
               return true;
             }
           },
           child: Scaffold(
             backgroundColor: Colors.white,
             body: Container(
                 child: SafeArea(
                     child: Column(
                       children: [
                         Row(
                           mainAxisAlignment: MainAxisAlignment.spaceAround,
                           children: [
                             back(context, 0, 0),
                             Expanded(
                               child: Container(
                                   margin: EdgeInsets.only(bottom: 40),
                                   child: AuthHeader(widget.title)),
                             ),
                             myAccount(context)
                           ],
                         ),
                         viewIntOrExt3D(context),
                         Expanded(
                           child: Container(
                             alignment: Alignment.center,
                             margin: EdgeInsets.only(top: 40),
                             child: WebViewPlus(
                               initialUrl: "",
                               javascriptMode: JavascriptMode.unrestricted,
                               onWebViewCreated: (controller) {
                                 _controller = controller;
                                 _controller.loadUrl(widget.file);
                               },
                               javascriptChannels: <JavascriptChannel>{
                                 JavascriptChannel(
                                     name: 'MessageInvoker',
                                     onMessageReceived: (JavascriptMessage message) {
                                       performAction(context, Strings.pdf, Strings.fromHotSpot,  page: 53);
                                     }),
                                 JavascriptChannel(
                                     name: 'MessageGetFromFoot',
                                     onMessageReceived: (JavascriptMessage message) {
                                       performAction(context, Strings.pdf, Strings.fromHotSpot ,page: 55);
                                     }),
                                 JavascriptChannel(
                                     name: 'MessageFromFace',
                                     onMessageReceived: (JavascriptMessage message) {
                                       performAction(context, Strings.pdf, Strings.fromHotSpot ,page: 57);
                                     }),
                                 JavascriptChannel(
                                     name: 'Walls',
                                     onMessageReceived: (JavascriptMessage message) {
                                       performAction(context, Strings.pdf, Strings.fromHotSpot ,page: 67);
                                     }),
                                 JavascriptChannel(
                                     name: 'SolidWalls',
                                     onMessageReceived: (JavascriptMessage message) {
                                       performAction(context, Strings.pdf, Strings.fromHotSpot ,page: 103);
                                     }),
                                 JavascriptChannel(
                                     name: 'Drainage',
                                     onMessageReceived: (JavascriptMessage message) {
                                       performAction(context, Strings.pdf, Strings.fromHotSpot ,page: 76);
                                     }),
                                 JavascriptChannel(
                                     name: 'Foundation',
                                     onMessageReceived: (JavascriptMessage message) {
                                       performAction(context, Strings.pdf, Strings.fromHotSpot ,page: 26);
                                     }),
                                 JavascriptChannel(
                                     name: 'Chimney',
                                     onMessageReceived: (JavascriptMessage message) {
                                       performAction(context, Strings.pdf, Strings.fromHotSpot ,page: 122);
                                     }),
                                 JavascriptChannel(
                                     name: 'Glazing',
                                     onMessageReceived: (JavascriptMessage message) {
                                       performAction(context, Strings.pdf, Strings.fromHotSpot ,page: 174);
                                     }),
                                 JavascriptChannel(
                                     name: 'Floor',
                                     onMessageReceived: (JavascriptMessage message) {
                                       performAction(context, Strings.pdf, Strings.fromHotSpot ,page: 90);
                                     }),
                                 JavascriptChannel(
                                     name: 'Roof',
                                     onMessageReceived: (JavascriptMessage message) {
                                       performAction(context, Strings.pdf, Strings.fromHotSpot ,page: 144);
                                     }),
                               },
                             ),
                           ),
                         )
                       ],
                     ))),
             bottomSheet: Container(color: Colors.white, child: actionOn3D()),
             bottomNavigationBar: bottomSheetDashboard(context, Strings.from3DScreen),
           ));
     }
  }

  Widget actionOn3D() {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Bouncing(
              child: actions(Strings.ic_zoom_in, 22),
              onPress: () {
                _controller.webViewController.runJavascript("zoomIn()");
              }),
          Bouncing(
              child: IconButton(
                  onPressed: () {}, icon: Image.asset(Strings.ic_zoom_out)),
              onPress: () {
                _controller.webViewController.runJavascript("zoomOut()");
              }),
          Bouncing(
              child: actions(Strings.ic_rotate, 42),
              onPress: () {
                _controller.webViewController.runJavascript("move()");
              }),
          Bouncing(
              child: actions(Strings.ic_search, 22),
              onPress: () {
                Navigator.of(context).push(CupertinoPageRoute(
                    builder: (BuildContext context) =>
                        const TitlesListScreen()));
              }),
          Bouncing(
              child: actions(Strings.ic_expand, 22),
              onPress: () {
                Navigator.of(context).push(CupertinoPageRoute(
                    builder: (BuildContext context) =>
                        ModelFullScreen(path: widget.file,)));
              }),
        ],
      ),
    );
  }
}
