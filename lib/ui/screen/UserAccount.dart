import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:icw_sample/Utils/SharedStorage.dart';
import 'package:icw_sample/ui/widgets/CustomWidgets.dart';
import 'package:no_internet_check/internet_connectivity/initialize_internet_checker.dart';
import 'package:provider/provider.dart';
import 'package:tuple/tuple.dart';
import '../../Utils/Strings.dart';
import '../../provider/AccountProvider.dart';
import '../widgets/Buttons.dart';
import '../widgets/InputField.dart';
import '../widgets/Text.dart';

class UserAccount extends StatefulWidget {
  const UserAccount({
    Key? key,
  }) : super(key: key);

  @override
  _UserAccount createState() => _UserAccount();
}

class _UserAccount extends State<UserAccount> {
  final TextEditingController _email = TextEditingController();
  final TextEditingController _userName = TextEditingController();
  final TextEditingController _name = TextEditingController();
  final TextEditingController _confirmPass = TextEditingController();
  final TextEditingController _password = TextEditingController();
  final TextEditingController _changePassword = TextEditingController();
  final FocusNode _focusNode = FocusNode();
  final FocusNode _focusNodeName = FocusNode();
  final FocusNode _focusNodeUserPass = FocusNode();
  final FocusNode _focusNodeChangePass = FocusNode();
  final FocusNode _focusNodeUserConfirmPass = FocusNode();
  final FocusNode _focusNodeEmail = FocusNode();
  late Color color = Colors.white70;

  @override
  void initState() {
    InternetChecker();
    SharedStorage.read(Strings.PASSWORD_LABEL).then((value) =>  {
      _password.text = jsonDecode(value),
      _changePassword.text = jsonDecode(value),
      _confirmPass.text = jsonDecode(value),
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _focusNode.addListener(() {
      setState(() {
        color = _focusNode.hasFocus ? Colors.blue : Colors.red;
      });
    });

    _focusNodeChangePass.addListener(() {
      setState(() {
        color = _focusNodeChangePass.hasFocus ? Colors.blue : Colors.red;
      });
    });

    _focusNodeName.addListener(() {
      setState(() {
        color = _focusNode.hasFocus ? Colors.blue : Colors.red;
      });
    });

    _focusNodeUserPass.addListener(() {
      setState(() {
        color = _focusNode.hasFocus ? Colors.blue : Colors.red;
      });
    });

    _focusNodeUserConfirmPass.addListener(() {
      setState(() {
        color = _focusNode.hasFocus ? Colors.blue : Colors.red;
      });
    });

    _focusNodeEmail.addListener(() {
      setState(() {
        color = _focusNode.hasFocus ? Colors.blue : Colors.red;
      });
    });

    return ChangeNotifierProvider<AccountProvider>(
        create: (context) => AccountProvider(),
        child: Builder(builder: (context) {
          return WillPopScope(
            onWillPop: () async {
              if (Navigator
                  .of(context)
                  .userGestureInProgress) {
                return false;
              } else {
                return true;
              }
            },
            child: Scaffold(
              body: SingleChildScrollView(
                  child: SafeArea(
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          appBar(context, Strings.MY_ACCOUNT),
                          Consumer<AccountProvider>(builder: (context, provider, child) {
                            _name.text = provider.response?.data?.displayName ?? "";
                            return InputFieldAccUpdate(
                                _name, Strings.NAME, Strings.ic_user,
                                _focusNode, color, Strings.ic_edit, provider: provider, i: 1);
                          }),
                          Consumer<AccountProvider>(builder: (context, provider, child) {
                            _userName.text = provider.response?.data?.userLogin! ?? "";
                            return InputFieldAccUpdate(_userName, Strings.USERNAME_LABEL,
                                Strings.ic_key, _focusNodeName, color,
                                Strings.ic_edit,  provider: provider, i: 2);
                          }),
                          Consumer<AccountProvider>(builder: (context, provider, child) {
                            _email.text = provider.response?.data?.userEmail ?? "";
                            return InputFieldAccUpdate(
                                _email, Strings.EMAIL_LABEL, Strings.ic_email,
                                _focusNodeEmail, color, Strings.ic_edit,  provider: provider, i: 3);
                          }),
                          InputFieldAccUpdate(_password, Strings.PASSWORD_LABEL,
                              Strings.ic_lock, _focusNodeUserPass, color,
                              Strings.ic_edit),
                          const SizedBox(
                            height: 50,
                          ),
                          Container(
                            margin: const EdgeInsets.only(left: 30),
                            child: Align(
                              alignment: Alignment.centerLeft,
                              child:
                              anyText(Strings.CHANGE_PASSWORD, 14, 0, 0,
                                  FontWeight.bold),
                            ),
                          ),
                          Consumer<AccountProvider>(builder: (context, provider, child) {
                            String img = provider.userPassword == true
                                ? Strings.ic_hide_pass
                                : Strings.ic_pass_show;
                            return InputField(_changePassword, Strings.PASSWORD_LABEL, Strings.ic_lock,
                                _focusNodeChangePass, color,
                                provider: provider, passShowOrHide: img);
                          }),
                          Consumer<AccountProvider>(builder: (context, provider, child) {
                            String img = provider.userConfirmPassword == true
                                ? Strings.ic_hide_pass
                                : Strings.ic_pass_show;
                            return InputField(_confirmPass, Strings.CONFIRM_PASSWORD_LABEL,
                                Strings.ic_confirm_lock, _focusNodeUserConfirmPass, color,
                                provider: provider, passShowOrHide: img);
                          }),
                          Consumer<AccountProvider>(
                              builder: (context, provider, child) {
                                var t = Tuple5<
                                    TextEditingController,
                                    TextEditingController,
                                    TextEditingController,
                                    TextEditingController,
                                    TextEditingController>(
                                    _name, _userName, _email, _changePassword,
                                    _confirmPass);
                                return Column(children:
                                [loginButton(context,
                                    Strings.SAVE, Strings.margin_fourty,
                                    Strings.margin_fifty, provider,
                                    signUpControllers: t),
                                  provider.isValid ? const Text("Updated") : Container()
                                ],
                                );
                              })
                        ],
                      ))),
            ),
          );
        }));
  }
}