import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:icw_sample/Utils/Toast.dart';
import 'package:icw_sample/models/GenericResponse.dart';
import 'package:icw_sample/provider/LoginProvider.dart';
import 'package:icw_sample/streams/ButtonStreams.dart';
import 'package:icw_sample/ui/screen/Dashboard.dart';
import 'package:icw_sample/ui/widgets/Text.dart';
import '../../Utils/Helper.dart';
import '../../Utils/Strings.dart';
import '../../enum/ApplicationEnums.dart';
import '../../interfaces/ButtonClicksListener.dart';
import '../../models/LoginUser.dart';
import '../../repository/ICWApi.dart';
import '../widgets/Buttons.dart';
import '../widgets/Dialog.dart';
import '../widgets/Images.dart';
import '../widgets/InputField.dart';
import 'ForgotPassword.dart';
import 'SignUp.dart';
import 'package:provider/provider.dart';
import 'package:tuple/tuple.dart';
import 'package:no_internet_check/no_internet_check.dart';


class Login extends StatefulWidget {
  const Login({
    Key? key,
  }) : super(key: key);

  @override
  _Login createState() => _Login();
}

class _Login extends State<Login> implements Listen {
  final TextEditingController _email = TextEditingController();
  final TextEditingController _password = TextEditingController();
  bool userSaved = false;
  final FocusNode _focusNode = FocusNode();
  final FocusNode _focusNodePass = FocusNode();
  late Color color = Colors.white70;

  checkToken() async {
    // LoginUserResponse user =
    // LoginUserResponse.fromJson(await SharedPref().read("user"));
    // if (user.clinicName != null) {
    //   Navigator.of(context).push(
    //     CupertinoPageRoute(builder: (BuildContext context) => DocDashboard()),
    //   );
    // } else {
    //   LoginPatientResponse userPatient =
    //   LoginPatientResponse.fromJson(await SharedPref().read("userPatient"));
    //   Navigator.of(context).push(
    //     CupertinoPageRoute(builder: (BuildContext context) => ShowUserMeditationOrBookSession()),
    //   );
    //   if (userPatient.name != null) {
    //     setState(() {
    //       userSaved = true;
    //     });
    //   }
    // }
  }

  @override
  void initState() {
    InternetChecker();
    checkToken();
    super.initState();
    Future.delayed(const Duration(microseconds: 1000),() {
      // checkConnectivity(context);
    });
  }

  @override
  Widget build(BuildContext context) {
    _focusNode.addListener(() {
      setState(() {
        color = _focusNode.hasFocus ? Colors.blue : Colors.red;
      });
    });

    return ChangeNotifierProvider<LoginProvider>(
        create: (context) => LoginProvider(),
        child: Builder(builder: (context) {
          return WillPopScope(
            onWillPop: () async {
              if (Navigator.of(context).userGestureInProgress) {
                return false;
              } else {
                return true;
              }
            },
            child: Scaffold(
              body: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    AuthHeader(Strings.LOGIN),
                    loginLogo(context, 50, 100, 100, Strings.margin_eighty),
                    InputField(_email, Strings.LOGIN_LABEL, Strings.ic_email, _focusNode, color),
                    Consumer<LoginProvider>(builder: (context, provider, child) {
                      String img = provider.userPassword == true ? Strings.ic_hide_pass : Strings.ic_pass_show;
                      return   InputField(_password, Strings.PASSWORD_LABEL, Strings.ic_lock, _focusNodePass, color, provider: provider, passShowOrHide: img);
                    }),
                    Consumer<LoginProvider>(builder: (context, provider, child){
                      var t = Tuple2<TextEditingController, TextEditingController>(_email,_password);
                      return Column(
                        children: [
                          loginButton(context, Strings.LOGIN, 40, 40, provider, controllers: t),
                          provider.userIsValid ? Text("User Valid") : Container(),
                      ],
                      );
                    }),
                    forgotPassword(),
                  ],
                ),
              ),
              bottomSheet: noAccount(),
            ),
          );
        }));
  }

  Container forgotPassword() {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: GestureDetector(
        onTap: () {
          Navigator.of(context).push(
            CupertinoPageRoute(
                builder: (BuildContext context) => const ForgotPassword()),
          );
        },
        child: Text(
          Strings.FORGOT_PASS,
        ),
      ),
      alignment: Alignment.center,
    );
  }

  Widget noAccount() {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: ElevatedButton(
        onPressed: () {
          Navigator.of(context).push(
            CupertinoPageRoute(
                builder: (BuildContext context) => const SignUp()),
          );
        },
        style: ElevatedButton.styleFrom(
            primary: Color(Strings.SKY_COLOR),
            padding: const EdgeInsets.all(25)),
        child: const Text(Strings.NOT_REGISTERED_CREATE_ACCOUNT),
      ),
    );
  }

  @override
  void onClick(String from) {
    ShowToast.showToastApp(from);
  }

//
// saveUserData(GenericResponse value) {
//   SharedPref sharedPref = SharedPref();
//   value.password = _password.text.trim();
//   sharedPref.save("user", value);
//   ShowToast.showToastApp("Login Successful", context);
//   Navigator.pop(context);
// }
}
