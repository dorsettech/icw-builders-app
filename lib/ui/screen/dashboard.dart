import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:icw_sample/ui/widgets/Text.dart';
import 'package:no_internet_check/internet_connectivity/initialize_internet_checker.dart';
import '../../Utils/Helper.dart';
import '../../Utils/Strings.dart';
import '../widgets/Buttons.dart';
import '../widgets/CustomWidgets.dart';
import '../widgets/Images.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({
    Key? key,
  }) : super(key: key);

  @override
  _Dashboard createState() => _Dashboard();
}

class _Dashboard extends State<Dashboard> with WidgetsBindingObserver {


  @override
  void initState() {
    InternetChecker();
    super.initState();
    WidgetsBinding.instance!.addObserver(this);
  }

  @override
  void dispose() {
    WidgetsBinding.instance!.removeObserver(this);
    super.dispose();

  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);

    switch (state) {
      case AppLifecycleState.resumed:
        break;
      case AppLifecycleState.inactive:
        break;
      case AppLifecycleState.paused:
        break;
      case AppLifecycleState.detached:
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        if (Navigator.of(context).userGestureInProgress) {
          return false;
        } else {
          return true;
        }
      },
      child: Scaffold(
          bottomNavigationBar: bottomSheetDashboard(context, Strings.fromHome),
        body: homeMainScreen(),
      ),
    );
  }

  Widget homeMainScreen() {
    return SingleChildScrollView(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  back(context, 0, 0),
                  Expanded(
                    child: loginLogo(
                        context,
                        Strings.margin_fifty,
                        Strings.margin_thirty,
                        Strings.margin_thirty,
                        Strings.margin_fifty),
                  ),
                  myAccount(context)
                ],
              ),
              anyText(Strings.KEY_MEASURE, 20, 40, 0, FontWeight.bold),
              anyText(Strings.ICW_MANUAL, 18, 8, 0, FontWeight.bold),
              Container(
                margin: const EdgeInsets.only(top: 8),
                width: MediaQuery.of(context).size.width - 50,
                  child: const Text(
                Strings.KEY_ICW_DETAIL,
                textAlign: TextAlign.center,
                style: TextStyle(fontSize: 14, fontWeight: FontWeight.normal),
              )),
              Container(
                margin: const EdgeInsets.only(top: 8),
                  width: MediaQuery.of(context).size.width - 50,
                  child: Row(
                    children: [
                      item(Strings.home, Strings.HOME),
                      item(Strings.flats, Strings.FLATS),
                    ],
                  )),
              Container(
                  // width: MediaQuery.of(context).size.width - 50,
                margin: const EdgeInsets.only(right: 50),
                  child: Row(
                    children: [
                      item(Strings.conversation, Strings.CONVERSATION),
                      item(Strings.pdf, Strings.PDF_READER),
                    ],
                  )),
            ]));
  }

  Widget item(String image, String text) {
    return Expanded(
        child: Bouncing(
          child: Stack(
            alignment: Alignment.topCenter,
            children: [bg(), dashboardImages(image, text)],
          ),
          onPress: () => performAction(context, image, Strings.fromHome),
        )
    );
  }
}