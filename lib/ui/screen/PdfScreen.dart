import 'dart:async';
import 'dart:io';
import 'dart:ui';

import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:no_internet_check/internet_connectivity/initialize_internet_checker.dart';
import 'package:path_provider/path_provider.dart';
import 'package:flutter_pdfview/flutter_pdfview.dart';
import 'package:provider/provider.dart';

import '../../Utils/LocalStore.dart';
import '../../Utils/SharedStorage.dart';
import '../../Utils/Strings.dart';
import '../../provider/PdfProvider.dart';
import '../widgets/Buttons.dart';
import '../widgets/CustomWidgets.dart';
import '../widgets/Images.dart';
import '../widgets/Text.dart';


class PDFScreen extends StatefulWidget {
  int? page;
  PDFScreen({Key? key, this.page}) : super(key: key);

  _PDFScreenState createState() => _PDFScreenState();
}

class _PDFScreenState extends State<PDFScreen> with WidgetsBindingObserver {
  late String? url;
  String remotePDFpath = "";
  late PDFViewController _controller = PDFViewController as PDFViewController;
  int? pages = 0;
  int? currentPage = 0;
  bool isReady = false;
  String errorMessage = '';

  @override
  void initState() {
    super.initState();
    LocalStore.tabPos = 3;
    fromAsset('assets/pdfs/icw.pdf', 'icw.pdf').then((f) {
      setState(() {
        remotePDFpath = f.path;
      });
    });

    // createFileOfPdfUrl().then((f) {
    //   setState(() {
    //     remotePDFpath = f.path;
    //   });
    // });

    getCurrentPage();
  }


  void getCurrentPage() {
    if (widget.page == null)
    {
      Future<dynamic> page = SharedStorage.readInt(Strings.BOOK_MARK);
      page.then((value) {
        setState(() {
          currentPage = int.parse(value.toString());
        });
      });
    } else
    {
      setState(() {
        currentPage = widget.page;
      });
    }

  }


  @override
  void dispose() {
    LocalStore.tabPos = -1;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // checkConnectivity(context);
    return ChangeNotifierProvider<PDFProvider>(
        create: (context) => PDFProvider(),
        child: Builder(builder: (context) {
          return WillPopScope(
              onWillPop: () async {
                if (Navigator.of(context).userGestureInProgress) {
                  return false;
                } else {
                  return true;
                }
              },
              child: Scaffold(
                body: Stack(
                  children: <Widget>[
                    remotePDFpath != ""
                        ?
                    PDFView(
                            filePath: remotePDFpath,
                            enableSwipe: true,
                            swipeHorizontal: true,
                            autoSpacing: false,
                            pageFling: true,
                            pageSnap: true,
                            defaultPage: currentPage!,
                            fitPolicy: FitPolicy.BOTH,
                            preventLinkNavigation: false,
                            onRender: (_pages) {
                              setState(() {
                                pages = _pages;
                                isReady = true;
                              });
                            },
                            onError: (error) {
                              setState(() {
                                errorMessage = error.toString();
                              });
                              print(error.toString());
                            },
                            onPageError: (page, error) {
                              setState(() {
                                errorMessage = '$page: ${error.toString()}';
                              });
                              print('$page: ${error.toString()}');
                            },
                            onViewCreated:
                                (PDFViewController pdfViewController) {
                              _controller = pdfViewController;
                            },
                            onLinkHandler: (String? uri) {
                              print('goto uri: $uri');
                            },
                            onPageChanged: (int? page, int? total) {
                              print('page change: $page/$total');
                              setState(() {
                                currentPage = page;
                              });
                            },
                          )
                        : Container(),
                    Align(
                      alignment: Alignment.topCenter,
                      child: Container(
                        margin: const EdgeInsets.only(top: 30),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            back(context, 0, 0),
                            Expanded(
                              child: Container(
                                  margin: const EdgeInsets.only(bottom: 40),
                                  child: AuthHeader(Strings.PDF_OPERATIONS)),
                            ),
                            myAccount(context)
                          ],
                        ),
                      ),
                    ),
                    errorMessage.isEmpty
                        ? !isReady
                            ? const Center(
                                child: CircularProgressIndicator(),
                              )
                            : Container()
                        : Center(
                            child: Text(errorMessage),
                          ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: Container(
                          margin: const EdgeInsets.only(bottom: 70),
                          child: anyText(
                              "Page $currentPage", 14, 0, 0, FontWeight.bold)),
                    ),
                    Align(
                      alignment: Alignment.bottomCenter,
                      child: actionOnPDF(),
                    ),
                  ],
                ),
                bottomNavigationBar: bottomSheetDashboard(context, Strings.fromPDF),
              ));
        }));
  }

  Widget actionOnPDF() {
    return Container(
      margin: EdgeInsets.only(bottom: 30),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          // Bouncing(child: actions(Strings.ic_expand, 22), onPress: () {}),
          Bouncing(
              child: actions(Strings.ic_back, 22),
              onPress: () {
                setPage(currentPage! - 1);
              }),
          Bouncing(
              child: actions(Strings.ic_next, 22),
              onPress: () {
                setPage(currentPage! + 1);
              }),
          // Bouncing(
          //     child: Icon(Icons.bookmark_add_outlined, color: Color(Strings.SKY_COLOR),),
          //     onPress: () {
          //       SharedStorage.saveInt(Strings.BOOK_MARK, currentPage);
          //     }),
          // SizedBox(width: 4),
        ],
      ),
    );
  }

  Future<File> createFileOfPdfUrl() async {
    Completer<File> completer = Completer();
    print("Start download file from internet!");
    try {
      url = Strings.url;
      final filename = url?.substring(url!.lastIndexOf("/") + 1);
      var request = await HttpClient().getUrl(Uri.parse(url!));
      var response = await request.close();
      var bytes = await consolidateHttpClientResponseBytes(response);
      var dir = await getApplicationDocumentsDirectory();
      print("Download files");
      print("${dir.path}/$filename");
      File file = File("${dir.path}/$filename");

      await file.writeAsBytes(bytes, flush: true);
      completer.complete(file);
    } catch (e) {
      throw Exception('Error parsing asset file!');
    }

    return completer.future;
  }

  Future<File> fromAsset(String asset, String filename) async {
    // To open from assets, you can copy them to the app storage folder, and the access them "locally"
    Completer<File> completer = Completer();

    try {
      var dir = await getApplicationDocumentsDirectory();
      File file = File("${dir.path}/$filename");
      var data = await rootBundle.load(asset);
      var bytes = data.buffer.asUint8List();
      await file.writeAsBytes(bytes, flush: true);
      completer.complete(file);
    } catch (e) {
      throw Exception('Error parsing asset file!');
    }

    return completer.future;
  }

  void setPage(int i) {
    var page = i;
    setState(() {
      currentPage = page;
    });
    _controller.setPage(page);
  }
}