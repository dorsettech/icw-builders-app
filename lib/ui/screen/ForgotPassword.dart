import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:icw_sample/interfaces/ButtonClicksListener.dart';
import 'package:icw_sample/provider/EmailProvider.dart';
import 'package:icw_sample/ui/widgets/CustomWidgets.dart';
import 'package:icw_sample/ui/widgets/Text.dart';
import 'package:no_internet_check/internet_connectivity/initialize_internet_checker.dart';
import 'package:provider/provider.dart';
import '../../Utils/Helper.dart';
import '../../Utils/Strings.dart';
import '../widgets/Buttons.dart';
import '../widgets/Images.dart';
import '../widgets/InputField.dart';

class ForgotPassword extends StatefulWidget {
  const ForgotPassword({
    Key? key,
  }) : super(key: key);

  @override
  _ForgotPassword createState() => _ForgotPassword();
}

class _ForgotPassword extends State<ForgotPassword> {
  final TextEditingController _email = TextEditingController();
  final FocusNode _focusNode = FocusNode();
  late Color color = Colors.white70;

  @override
  void initState() {
    InternetChecker();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    _focusNode.addListener(() {
      setState(() {
        color = _focusNode.hasFocus ? Colors.blue : Colors.red;
      });
    });

    return ChangeNotifierProvider<EmailProvider>(
        create: (context) => EmailProvider(),
        child: Builder(builder: (context) {
          return WillPopScope(
            onWillPop: () async {
              if (Navigator.of(context).userGestureInProgress) {
                return false;
              } else {
                return true;
              }
            },
            child: Scaffold(
              body: SingleChildScrollView(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    appBar(context, Strings.RESET_PASS),
                    loginLogo(context, 0, 100, 100, Strings.margin_eighty),
                    InputField(_email, Strings.EMAIL_LABEL, Strings.ic_email,
                        _focusNode, color),
                    Consumer<EmailProvider>(builder: (context, provider, child){
                      return loginButton(context, Strings.SUBMIT, 0, 40, provider, emailController: _email);

                    }),
                  ],
                ),
              ),
            ),
          );
        }));
  }

// void validation() {
//   if (_email.text.isNotEmpty && _password.text.isNotEmpty) {
//     if (Validator.emailValid(_email.text)) {
//       if (_password.text.length > 5) {
//         if (patient)
//           loginPatient();
//         else
//           loginDoctor();
//       } else {
//         loginDoctor();
//         ShowToast.showToastApp(Strings.passwordShort, context);
//       }
//     } else {
//       ShowToast.showToastApp(Strings.emailNotValid, context);
//     }
//   } else {
//     ShowToast.showToastApp(Strings.fillAllFields, context);
//   }
// }

// void loginPatient() {
//   showDialog(
//     context: context,
//     barrierDismissible: false,
//     builder: (BuildContext context) {
//       return Spinner.spinkit;
//     },
//   );
//   var data = LoginUser(_password.text.trim(), _email.text.trim());
//   final client =
//   LoginClient(Dio(BaseOptions(contentType: "application/json")));
//   client.loginUser(jsonEncode(data.toJson())).then((value) async {
//     savePatientData(value);
//     Navigator.of(context).pushReplacement(
//       CupertinoPageRoute(builder: (BuildContext context) => ShowUserMeditationOrBookSession()),
//     );
//   }).catchError((Object obj) {
//     Navigator.pop(context);
//     switch (obj.runtimeType) {
//       case DioError:
//         onError(obj);
//         break;
//       default:
//     }
//   });
// }

// onError(Object obj) {
//   final res = (obj as DioError).response;
//   if (res.statusCode == 404) {
//     ShowToast.showToastApp("error");
//   } else {
//     ShowToast.showToastApp(res.data["message"]);
//   }
// }

// saveUserData(LoginUserResponse value) {
//   SharedPref sharedPref = SharedPref();
//   value.password = _password.text.trim();
//   sharedPref.save("user", value);
//   ShowToast.showToastApp("Login Successful", context);
//   Navigator.pop(context);
// }
}
