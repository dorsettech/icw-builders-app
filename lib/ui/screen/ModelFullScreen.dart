import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:icw_sample/ui/widgets/CustomWidgets.dart';
import 'package:no_internet_check/internet_connectivity/initialize_internet_checker.dart';
import 'package:webview_flutter/webview_flutter.dart';
import 'package:webview_flutter_plus/webview_flutter_plus.dart';

import '../../Utils/Helper.dart';
import '../../Utils/LocalStore.dart';
import '../../Utils/Strings.dart';
import '../widgets/Buttons.dart';
import 'PdfScreen.dart';
import 'TitlesListScreen.dart';

class ModelFullScreen extends StatefulWidget {
  final VoidCallback? onFlip;
  final String path;

  const ModelFullScreen({Key? key, this.onFlip, required this.path}) : super(key: key);

  @override
  _ModelFullScreen createState() => _ModelFullScreen();
}

class _ModelFullScreen extends State<ModelFullScreen> with SingleTickerProviderStateMixin {
  late WebViewPlusController _controller;

  @override
  void initState() {
    InternetChecker();
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async {
          if (Navigator.of(context).userGestureInProgress) {
            return false;
          } else {
            return true;
          }
        },
        child: Scaffold(
          backgroundColor: Colors.white,
          body: Stack(
            children: [
              Container(
                alignment: Alignment.center,
                margin: const EdgeInsets.only(top: 40),
                child: WebViewPlus(
                  initialUrl: widget.path,
                  javascriptMode: JavascriptMode.unrestricted,
                  onWebViewCreated: (controller) {
                    _controller = controller;
                  },
                  javascriptChannels: <JavascriptChannel>{
                    JavascriptChannel(
                        name: 'MessageInvoker',
                        onMessageReceived: (JavascriptMessage message) {
                          performAction(context, Strings.pdf, Strings.fromHotSpot,  page: 53);
                        }),
                    JavascriptChannel(
                        name: 'MessageGetFromFoot',
                        onMessageReceived: (JavascriptMessage message) {
                          performAction(context, Strings.pdf, Strings.fromHotSpot ,page: 55);
                        }),
                    JavascriptChannel(
                        name: 'MessageFromFace',
                        onMessageReceived: (JavascriptMessage message) {
                          performAction(context, Strings.pdf, Strings.fromHotSpot ,page: 57);
                        }),
                    JavascriptChannel(
                        name: 'Walls',
                        onMessageReceived: (JavascriptMessage message) {
                          performAction(context, Strings.pdf, Strings.fromHotSpot ,page: 67);
                        }),
                    JavascriptChannel(
                        name: 'SolidWalls',
                        onMessageReceived: (JavascriptMessage message) {
                          performAction(context, Strings.pdf, Strings.fromHotSpot ,page: 103);
                        }),
                    JavascriptChannel(
                        name: 'Drainage',
                        onMessageReceived: (JavascriptMessage message) {
                          performAction(context, Strings.pdf, Strings.fromHotSpot ,page: 76);
                        }),
                    JavascriptChannel(
                        name: 'Foundation',
                        onMessageReceived: (JavascriptMessage message) {
                          performAction(context, Strings.pdf, Strings.fromHotSpot ,page: 26);
                        }),
                    JavascriptChannel(
                        name: 'Chimney',
                        onMessageReceived: (JavascriptMessage message) {
                          performAction(context, Strings.pdf, Strings.fromHotSpot ,page: 122);
                        }),
                    JavascriptChannel(
                        name: 'Glazing',
                        onMessageReceived: (JavascriptMessage message) {
                          performAction(context, Strings.pdf, Strings.fromHotSpot ,page: 174);
                        }),
                    JavascriptChannel(
                        name: 'Floor',
                        onMessageReceived: (JavascriptMessage message) {
                          performAction(context, Strings.pdf, Strings.fromHotSpot ,page: 90);
                        }),
                    JavascriptChannel(
                        name: 'Roof',
                        onMessageReceived: (JavascriptMessage message) {
                          performAction(context, Strings.pdf, Strings.fromHotSpot ,page: 144);
                        }),
                  },
                ),
              ),
              Container(
                margin: EdgeInsets.only(top: 50),
                child: back(context, 0, 0),
              )
            ],
          ),
          bottomSheet: Container(
              color: Colors.white,
              child: actionOn3D()),
        ));
  }

  Widget actionOn3D() {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceAround,
        children: [
          Bouncing(
              child: actions(Strings.ic_zoom_in, 22),
              onPress: () {
                _controller.webViewController.runJavascript("zoomIn()");
              }),
          Bouncing(
              child: IconButton(onPressed: () {}, icon: Image.asset(Strings.ic_zoom_out)),
              onPress: () {
                _controller.webViewController.runJavascript("zoomOut()");
              }),
          Bouncing(
              child: actions(Strings.ic_rotate, 42),
              onPress: () {
                _controller.webViewController.runJavascript("move()");
              }),
        ],
      ),
    );
  }
}
