import 'dart:math';

import 'package:confetti/confetti.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:icw_sample/provider/RegisterProvider.dart';
import 'package:no_internet_check/internet_connectivity/initialize_internet_checker.dart';
import 'package:provider/provider.dart';
import 'package:tuple/tuple.dart';
import '../../Utils/Strings.dart';
import '../animation/ConfetiAnimation.dart';
import '../animation/animated_check.dart';
import '../widgets/Buttons.dart';
import '../widgets/CustomWidgets.dart';
import '../widgets/Images.dart';
import '../widgets/InputField.dart';
import '../widgets/Text.dart';


class SignUp extends StatefulWidget {
  const SignUp({
    Key? key,
  }) : super(key: key);

  @override
  _SignUp createState() => _SignUp();
}

class _SignUp extends State<SignUp> with SingleTickerProviderStateMixin {
  final TextEditingController _email = TextEditingController();
  final TextEditingController _userName = TextEditingController();
  final TextEditingController _name = TextEditingController();
  final TextEditingController _confirmPass = TextEditingController();
  final TextEditingController _password = TextEditingController();
  final FocusNode _focusNode = FocusNode();
  final FocusNode _focusNodeName = FocusNode();
  final FocusNode _focusNodeUserPass = FocusNode();
  final FocusNode _focusNodeUserConfirmPass = FocusNode();
  final FocusNode _focusNodeEmail = FocusNode();
  late Color color = Colors.white70;
  late ConfettiController controllerTopCenter;
  late AnimationController _animationController;
  late Animation<double> _animation;

  @override
  void initState() {
    InternetChecker();
    super.initState();

    _animationController =
        AnimationController(vsync: this, duration: const Duration(seconds: 1));

    _animation = Tween<double>(begin: 0, end: 1).animate(
        CurvedAnimation(
            parent: _animationController, curve: Curves.easeInOutCirc));
    setState(() {
      initController();
    });
  }

  @override
  void dispose() {
    controllerTopCenter.dispose();
    _animationController.reset();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    _focusNode.addListener(() {
      setState(() {
        color = _focusNode.hasFocus ? Colors.blue : Colors.red;
      });
    });

    _focusNodeName.addListener(() {
      setState(() {
        color = _focusNode.hasFocus ? Colors.blue : Colors.red;
      });
    });

    _focusNodeUserPass.addListener(() {
      setState(() {
        color = _focusNode.hasFocus ? Colors.blue : Colors.red;
      });
    });

    _focusNodeUserConfirmPass.addListener(() {
      setState(() {
        color = _focusNode.hasFocus ? Colors.blue : Colors.red;
      });
    });

    _focusNodeEmail.addListener(() {
      setState(() {
        color = _focusNode.hasFocus ? Colors.blue : Colors.red;
      });
    });

    return ChangeNotifierProvider<RegisterProvider>(
        create: (context) => RegisterProvider(),
        child: Builder(builder: (context) {
          return WillPopScope(
            onWillPop: () async {
              if (Navigator.of(context).userGestureInProgress) {
                return false;
              } else {
                return true;
              }
            },
            child: Scaffold(
              body: SingleChildScrollView(
                child: Consumer<RegisterProvider>(
                    builder: (context, provider, child) {
                  var t = Tuple5<
                          TextEditingController,
                          TextEditingController,
                          TextEditingController,
                          TextEditingController,
                          TextEditingController>(
                      _name, _userName, _email, _password, _confirmPass);
                  if(provider.isValid) {
                      controllerTopCenter.play();
                      _animationController.forward();
                  }
                  return provider.isValid
                      ? registerSuccess()
                      : startRegistration(t, provider);
                }),
              ),
            ),
          );
        }));
  }

  Column registerSuccess() {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        buildConfettiWidget(controllerTopCenter, pi / 1),
        buildConfettiWidget(controllerTopCenter, pi / 4),
        AuthHeader(Strings.REGISTERATION_HEADER),
        success(),
        registrationSuccess(Strings.SUCCESS),
        anyText(Strings.SUCCESS_MESSAGE, 14, Strings.margin_twenty, 0,
            FontWeight.normal),
        anyText(Strings.SUCCESS_MESSAGE_DETAIL, 14, Strings.margin_thirty, 0,
            FontWeight.normal),
        loginButton(context, Strings.LETS_START, 0, Strings.margin_eighty, this)
      ],
    );
  }

  Column startRegistration(
      Tuple5<
              TextEditingController,
              TextEditingController,
              TextEditingController,
              TextEditingController,
              TextEditingController>
          t,
      RegisterProvider provider) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        appBar(context, Strings.SIGN_UP),
        loginLogo(context, 0, 100, 100, Strings.margin_eighty),
        InputField(_name, Strings.NAME, Strings.ic_user, _focusNode, color),
        InputField(_userName, Strings.USERNAME_LABEL, Strings.ic_key,
            _focusNodeName, color),
        InputField(_email, Strings.EMAIL_LABEL, Strings.ic_email,
            _focusNodeEmail, color),
        Consumer<RegisterProvider>(builder: (context, provider, child) {
          String img = provider.userPassword == true
              ? Strings.ic_hide_pass
              : Strings.ic_pass_show;
          return InputField(_password, Strings.PASSWORD_LABEL, Strings.ic_lock,
              _focusNodeUserPass, color,
              provider: provider, passShowOrHide: img);
        }),
        Consumer<RegisterProvider>(builder: (context, provider, child) {
          String img = provider.userConfirmPassword == true
              ? Strings.ic_hide_pass
              : Strings.ic_pass_show;
          return InputField(_confirmPass, Strings.CONFIRM_PASSWORD_LABEL,
              Strings.ic_confirm_lock, _focusNodeUserConfirmPass, color,
              provider: provider, passShowOrHide: img);
        }),
        loginButton(context, Strings.CREATE_ACCOUNT_LABEL,
            Strings.margin_fourty, Strings.margin_fifty, provider,
            signUpControllers: t)
      ],
    );
  }

  Container success() {
    return Container(
      margin: const EdgeInsets.all(20),
      padding: const EdgeInsets.all(10),
      height: 200,
      width: 200,
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(300),
          border: Border.all(width: 2, color: Color(Strings.LOGIN_COLOR))),
      child: AnimatedCheck(
        progress: _animation,
        size: 200,
        color: Color(Strings.LOGIN_COLOR),
      ),
    );
  }

  void initController() {
    controllerTopCenter =
        ConfettiController(duration: const Duration(seconds: 5));
  }
}