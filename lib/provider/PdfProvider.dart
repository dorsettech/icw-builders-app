import 'package:flutter/cupertino.dart';
import 'package:icw_sample/Utils/Validator.dart';
import 'package:tuple/tuple.dart';

import '../Utils/Strings.dart';
import '../Utils/Toast.dart';
import '../ui/screen/Dashboard.dart';

class PDFProvider extends ChangeNotifier {
  bool userIsValid = false;

  void checkUserValidity(
      Tuple2<TextEditingController, TextEditingController>? controllers,
      BuildContext context) {

    Navigator.of(context).push(
      CupertinoPageRoute(
          builder: (BuildContext context) => const Dashboard()),
    );
    //
    // String? email = controllers?.item1.value.text.toString();
    // String? password = controllers?.item2.value.text.toString();
    //
    // if (email?.isEmpty == true && password?.isEmpty == true) {
    //   ShowToast.showToastApp(Strings.fill_all_field);
    //   userIsValid = false;
    // }
    //
    // else if (Validator.checkEmail(email) && Validator.checkPassword(password)) {
    //   userIsValid = true;
    //   Navigator.of(context).push(
    //     CupertinoPageRoute(
    //         builder: (BuildContext context) => const Dashboard()),
    //   );
    // }
    //
    // else if (!Validator.checkPassword(password)) {
    //   ShowToast.showToastApp(Strings.invalid_password);
    //   userIsValid = false;
    // }
    //
    // else if (!Validator.checkEmail(email)) {
    //   userIsValid = false;
    //   ShowToast.showToastApp(Strings.invalid_email);
    // }
    notifyListeners();
  }
}
