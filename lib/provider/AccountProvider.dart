import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:icw_sample/Utils/SharedStorage.dart';
import 'package:icw_sample/enum/ApplicationEnums.dart';
import 'package:icw_sample/models/NonceResponse.dart';
import 'package:icw_sample/models/UserInfoResponse.dart';
import 'package:tuple/tuple.dart';

import '../Utils/Helper.dart';
import '../Utils/Strings.dart';
import '../Utils/Toast.dart';
import '../Utils/Validator.dart';
import '../models/UpdateUserResponse.dart';
import '../repository/ICWApi.dart';

class AccountProvider extends ChangeNotifier {
  bool isValid = false;
  bool userPassword = false;
  bool userConfirmPassword = false;
  User? response;
  late NonceResponse nonceResponse;

  AccountProvider() {
    getNonce();
    SharedStorage.read(Strings.USER_DATA).then((value) async {
      if (value != null) {
        User? user = UpdateUserResponse.fromJson(json.decode(value)).user;
        response = user;
        notifyListeners();
      } else {
        getCurrentUserData();
      }
    });
  }

  Future<void> getNonce() async {
    final client =
    IcwApi(Dio(BaseOptions(contentType: "application/json")));
    client.getNonce(Controller.users.name, Methods.update_user.name).then((value) async {
      nonceResponse = value;
      print("nonce is ->${value.nonce}");
    }).catchError((Object obj) {
      switch (obj.runtimeType) {
        case DioError:
          onError(obj);
          break;
        default:
      }
    });
  }

  void passVisibility({String? passShowOrHide}) {
    if (passShowOrHide == Strings.ic_pass_show) {
      userPassword = true;
    } else {
      userPassword = false;
    }
    notifyListeners();
  }

  void confirmPassVisibility({String? passShowOrHide}) {
    if (passShowOrHide == Strings.ic_pass_show) {
      userConfirmPassword = true;
    } else {
      userConfirmPassword = false;
    }
    notifyListeners();
  }

  void checkUserSignUpCredentials(
      BuildContext context,
      Tuple5<
              TextEditingController,
              TextEditingController,
              TextEditingController,
              TextEditingController,
              TextEditingController>?
          signUpControllers) {
    String? name = signUpControllers?.item1.value.text.toString();
    String? userName = signUpControllers?.item2.value.text.toString();
    String? email = signUpControllers?.item3.value.text.toString();
    String? password = signUpControllers?.item4.value.text.toString();
    String? confirmPass = signUpControllers?.item5.value.text.toString();

    if (email?.isEmpty == true &&
        password?.isEmpty == true &&
        userName?.isEmpty == true &&
        name?.isEmpty == true &&
        confirmPass?.isEmpty == true) {
      ShowToast.showToastApp(Strings.fill_all_field);
      isValid = false;
    } else if (!Validator.checkName(name)) {
      isValid = false;
      ShowToast.showToastApp(Strings.invalid_name);
    } else if (!Validator.checkEmail(email)) {
      isValid = false;
      ShowToast.showToastApp(Strings.invalid_email);
    } else if (password != confirmPass) {
      ShowToast.showToastApp(Strings.pass_not_match);
      isValid = false;
    } else if (!Validator.checkPassword(password)) {
      ShowToast.showToastApp(Strings.invalid_password);
      isValid = false;
    } else {
      // isValid = true;
      updateUserData(name, userName, email, password);
    }

    notifyListeners();
  }

  void getCurrentUserData() {
    final client = IcwApi(Dio(BaseOptions(contentType: "application/json")));
    client.getUser().then((value) async {
      response = value.user;
      notifyListeners();
    }).catchError((Object obj) {
      switch (obj.runtimeType) {
        case DioError:
          onError(obj);
          break;
        default:
      }
    });
  }

  void updateUserData(String? name,String? userName,String? email,String? password) {
    final client = IcwApi(Dio(BaseOptions(contentType: "application/json")));
    var data = FormData.fromMap({
      'u': Strings.adminName,
      'p': Strings.adminPass,
      'nonce': nonceResponse.nonce,
      'user_login': userName,
      'user_email': email,
      'user_nicename': name,
      'user_pass': password,
    });
    client.updateUser(data).then((value) async {
      isValid = true;
      response = value.user;
      SharedStorage.save(Strings.USER_DATA, value);
      SharedStorage.save(Strings.PASSWORD_LABEL, password);
      notifyListeners();
    }).catchError((Object obj) {
      switch (obj.runtimeType) {
        case DioError:
          onError(obj);
          break;
        default:
      }
    });
  }
}
