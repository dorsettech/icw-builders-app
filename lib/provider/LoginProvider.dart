import 'dart:convert';

import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:icw_sample/Utils/Validator.dart';
import 'package:tuple/tuple.dart';

import '../Utils/Helper.dart';
import '../Utils/Strings.dart';
import '../Utils/Toast.dart';
import '../models/LoginUser.dart';
import '../repository/ICWApi.dart';
import '../ui/screen/Dashboard.dart';
import '../ui/widgets/Dialog.dart';

class LoginProvider extends ChangeNotifier {
  bool userIsValid = false;
  bool userPassword = false;

  void passVisibility({String? passShowOrHide}) {
    if (passShowOrHide == Strings.ic_pass_show) {
      userPassword = true;
    } else {
      userPassword = false;
    }
    notifyListeners();
  }

  void checkUserValidity(
      Tuple2<TextEditingController, TextEditingController>? controllers,
      BuildContext context) {

    Navigator.of(context).push(
      CupertinoPageRoute(
          builder: (BuildContext context) => const Dashboard()),
    );
    return;

    String? email = controllers?.item1.value.text.toString();
    String? password = controllers?.item2.value.text.toString();

    if (email?.isEmpty == true && password?.isEmpty == true) {
      ShowToast.showToastApp(Strings.fill_all_field);
      userIsValid = false;
    }

    else if (email!.isNotEmpty && Validator.checkPassword(password)) {
      login(password, email, context);
    }

    // else if (!Validator.checkPassword(password)) {
    //   ShowToast.showToastApp(Strings.invalid_password);
    //   userIsValid = false;
    // }

    // else if (!Validator.checkEmail(email)) {
    //   userIsValid = false;
    //   ShowToast.showToastApp(Strings.invalid_email);
    // }
    notifyListeners();
  }

  void login(String? password, String? email, BuildContext context) {
    var data = FormData.fromMap({
      'user_login':email,
      'user_password':password
    });
    final client =
    IcwApi(Dio(BaseOptions(contentType: "application/json")));
    client.login(data).then((value) async {
      if (value.error != null) {
        ShowToast.showToastApp(value.error);
        return;
      }
      var data = FormData.fromMap({
        'username':email,
        'password':password
      });
      client.generateAuthCookie(data).then((value) async {
        if(value.status == "ok") {
          var data = FormData.fromMap({
          'cookie':value.cookie,
          });
          client.validateAuthCookie(data).then((value) async {
            if(value.valid == true) {
              userIsValid = true;
              Navigator.of(context).push(
                CupertinoPageRoute(
                    builder: (BuildContext context) => const Dashboard()),
              );
              notifyListeners();
            } else {
              ShowToast.showToastApp("Invalid User");
            }
          });
        }
      });
    }).catchError((Object obj) {
      // Navigator.pop(context);
      switch (obj.runtimeType) {
        case DioError:
          onError(obj);
          break;
        default:
      }
    });
  }
}