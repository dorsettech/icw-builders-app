
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:icw_sample/Utils/SharedStorage.dart';
import 'package:icw_sample/enum/ApplicationEnums.dart';
import 'package:icw_sample/models/NonceResponse.dart';
import 'package:tuple/tuple.dart';

import '../Utils/Helper.dart';
import '../Utils/Strings.dart';
import '../Utils/Toast.dart';
import '../Utils/Validator.dart';
import '../repository/ICWApi.dart';

class RegisterProvider extends ChangeNotifier {

  bool isValid = false;
  bool userPassword = false;
  bool userConfirmPassword = false;
  late NonceResponse response;

  RegisterProvider() {
    getNonce();
  }

  Future<void> getNonce() async {

    final client =
    IcwApi(Dio(BaseOptions(contentType: "application/json")));
    client.getNonce(Controller.user.name, Methods.register.name).then((value) async {
      response = value;
      print("nonce is ${response.nonce}");
    }).catchError((Object obj) {
      switch (obj.runtimeType) {
        case DioError:
          onError(obj);
          break;
        default:
      }
    });
  }

  void passVisibility({String? passShowOrHide}) {
    if (passShowOrHide == Strings.ic_pass_show) {
      userPassword = true;
    } else {
      userPassword = false;
    }
    notifyListeners();
  }

  void confirmPassVisibility({String? passShowOrHide}) {
    if (passShowOrHide == Strings.ic_pass_show) {
      userConfirmPassword = true;
    } else {
      userConfirmPassword = false;
    }
    notifyListeners();
  }

  void checkUserSignUpCredentials(BuildContext context, Tuple5<TextEditingController, TextEditingController, TextEditingController, TextEditingController, TextEditingController>? signUpControllers) {

    String? name = signUpControllers?.item1.value.text.toString();
    String? userName = signUpControllers?.item2.value.text.toString();
    String? email = signUpControllers?.item3.value.text.toString();
    String? password = signUpControllers?.item4.value.text.toString();
    String? confirmPass = signUpControllers?.item5.value.text.toString();

    if (email?.isEmpty == true
        && password?.isEmpty == true
        && userName?.isEmpty == true
        && name?.isEmpty == true
    && confirmPass?.isEmpty == true) {
      ShowToast.showToastApp(Strings.fill_all_field);
      isValid = false;
    }

    else if (!Validator.checkName(name)) {
      isValid = false;
      ShowToast.showToastApp(Strings.invalid_name);
    }

    else if (!Validator.checkEmail(email)) {
      isValid = false;
      ShowToast.showToastApp(Strings.invalid_email);
    }

    else if (password != confirmPass) {
      ShowToast.showToastApp(Strings.pass_not_match);
      isValid = false;
    }

    else if (!Validator.checkPassword(password)) {
      ShowToast.showToastApp(Strings.invalid_password);
      isValid = false;
    }
    else  {
      // isValid = true;
      signUp(userName, name, email, password, context);
    }
    
    notifyListeners();
  }

  void signUp(String? userName, String? name, String? email, String? password, BuildContext context) {
    var data = FormData.fromMap({
      'username': userName,
      'name': name,
      'email': email,
      'user_pass': password,
      'nonce': response.nonce,
    });
    final client =
    IcwApi(Dio(BaseOptions(contentType: "application/json")));
    client.signUp(data).then((value) async {
      if (value.status != "ok") {
        ShowToast.showToastApp("Invalid username or password");
        return;
      }
      print("register is ${value}");
      SharedStorage.save(Strings.PASSWORD_LABEL, password);
      isValid = true;
      notifyListeners();
    }).catchError((Object obj) {
      switch (obj.runtimeType) {
        case DioError:
          onError(obj);
          break;
        default:
      }
    });
  }
}