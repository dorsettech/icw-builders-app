import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';
import 'package:icw_sample/Utils/Validator.dart';
import 'package:tuple/tuple.dart';

import '../Utils/Helper.dart';
import '../Utils/Strings.dart';
import '../Utils/Toast.dart';
import '../repository/ICWApi.dart';
import '../ui/screen/Dashboard.dart';

class EmailProvider extends ChangeNotifier {
  bool validEmail = false;

  void checkEmailValidity(TextEditingController? controllers, BuildContext context) {

    String? email = controllers?.value.text.toString();

    if (email?.isEmpty == true) {
      ShowToast.showToastApp(Strings.enter_email);
      validEmail = false;
    }

    else if (!Validator.checkEmail(email)) {
      validEmail = false;
      ShowToast.showToastApp(Strings.invalid_email);
    }

    else if (Validator.checkEmail(email)) {
      resetPassword(email, context);
    }
    notifyListeners();
  }

  void resetPassword(String? email, BuildContext context) {
    final client = IcwApi(Dio(BaseOptions(contentType: "application/json")));
    var data = FormData.fromMap({
      'user_login': email,
    });
    client.resetPassword(data).then((value) async {
      if(value.error != null) {
        ShowToast.showToastApp(value.error);
        return;
      }
      validEmail = true;
      ShowToast.showToastApp(Strings.SUBMIT);
      Navigator.pop(context);
      notifyListeners();
    }).catchError((Object obj) {
      switch (obj.runtimeType) {
        case DioError:
          onError(obj);
          break;
        default:
      }
    });
  }
}
