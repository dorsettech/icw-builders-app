import '../Utils/Strings.dart';
import '../streams/ButtonStreams.dart';

class Listen{

  void onClick(String from) {
    ButtonStream.loggedInStream.stream.listen((event) {
      switch(event) {
        case Strings.LOGIN: from = Strings.LOGIN;
      }
    });

  }
}