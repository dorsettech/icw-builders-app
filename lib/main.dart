import 'dart:async';

import 'package:device_preview/device_preview.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:icw_sample/ui/screen/Dashboard.dart';
import 'package:icw_sample/ui/screen/Login.dart';
import 'package:icw_sample/ui/screen/PdfScreen.dart';
import 'package:no_internet_check/internet_connectivity/navigation_Service.dart';
import 'Utils/Strings.dart';

void main() {
  WidgetsFlutterBinding.ensureInitialized();

  //TODO:uncomment this code for multi device testing
  // runApp( DevicePreview(
  //   enabled: true,
  //   tools: const [
  //     ...DevicePreview.defaultTools,
  //   ],
  //   builder: (context) => const MyApp(),
  // ),);

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
      DeviceOrientation.portraitDown,
    ]);
    return MaterialApp(
      title: 'Model Viewer Demo',
      theme: ThemeData(
          inputDecorationTheme: InputDecorationTheme(
              enabledBorder: const UnderlineInputBorder(
                borderSide: BorderSide(color: Colors.grey),
                //  when the TextFormField in unfocused
              ) ,
              focusedBorder: UnderlineInputBorder(
                borderSide: BorderSide(color: Color(Strings.LOGIN_COLOR)),
                //  when the TextFormField in focused
              ),
              // labelStyle: TextStyle(color: Color(Strings.LOGIN_COLOR)),
            prefixIconColor: Color(Strings.LOGIN_COLOR)
          ),
        textSelectionTheme: TextSelectionThemeData(
          cursorColor: Color(Strings.LOGIN_COLOR),
        ),
      ),
      navigatorKey: NavigationService.navigationKey,
      initialRoute: '/',
      routes: {
        '/': (context) => const Login(),
        '/pdf': (context) => PDFScreen(),
        'dashboard': (context) => const Dashboard(),
      },
    );
  }
}
