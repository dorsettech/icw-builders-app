enum NetworkStatus {
  Online,
  Offline
}

enum Methods {
  update_user,
  register,
}

enum Controller {
  user,
  users
}